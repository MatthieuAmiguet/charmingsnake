from collections import deque

CT_PLAY, CT_OUT, CT_STOP, CT_CALL = range(4)

class Autoplayer:

    def __init__(self):
        
        self.start_list = deque()
        self.stop_list = deque()

    def play(self, obj, stopdelay=0):
        obj.stop()
        self.start_list.append((CT_PLAY, obj))
        self.stop_list.append((CT_STOP, (obj, stopdelay)))
        return obj
        
    def stop(self, obj, stopdelay=0):
        obj.stop()
        self.stop_list.append((CT_STOP, (obj, stopdelay)))
        return obj
        
    def out(self, obj, channel=0, inc=0, stopdelay=0):
        obj.stop()
        self.start_list.append((CT_OUT, (obj, channel, inc)))
        self.stop_list.append((CT_STOP, (obj, stopdelay)))
        return obj
    
    def call_on_start(self, f):
        self.start_list.append((CT_CALL, f))
        return f
        
    def call_on_stop(self, f):
        self.stop_list.append((CT_CALL, f))
        return f
