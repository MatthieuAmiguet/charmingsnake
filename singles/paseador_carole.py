import pyo
from foococo import foococo as fc
from utils.looping import LiveLooper
import context as ctx

server = pyo.Server(
    audio='jack',
    nchnls=2,
    ichnls=1,
    jackname='carole'
)

server.setJackAuto(False, False)
server.setJackAutoConnectInputPorts([
    ['system:capture_2'],
])
server.setJackAutoConnectOutputPorts([
    ['system:playback_1'],
    ['system:playback_2'],
])

ctx.server = server

fc.init(server, text="PSDR", model=2, device_index=2)
server.stop()

fader = pyo.SigTo(0, .1, 0)
mic = pyo.Input(0, mul=fader)


b0 = fc.MultiState(
        fc.button(0),
        [
            [fc.led_on(0, 'red'), lambda: fader.setValue(0)],
            [fc.led_on(0, 'green'), lambda: fader.setValue(1)],
        ],
    )

pedal = pyo.SigTo(fc.extension_pedal()**.7, 0.01)
loop_fader = pyo.SigTo(0, .1, 0)
loop_vol = pedal * loop_fader
loop = LiveLooper(mic, max_dur=20, xfade=.01, mul=loop_vol)

b1 = fc.MultiState(fc.button(1),
    [
        [fc.led_off(1), lambda: loop_fader.set('value',0, .1, lambda: loop.stop() and loop.looper.reset())],
        [fc.led_on(1, 'red'), loop.rec],
        [fc.led_on(1, 'green'), loop.stop_rec, loop.play, lambda: loop_fader.setValue(1)],
    ]
)

upf = pyo.TrigFunc(pyo.Thresh(pedal, 0.001, 0), loop.looper.loopnow)
#was panning .5 to left/right
pan = pyo.Pan(mic+loop, pan=0).out()

server.start()
