import pyo
from foococo import foococo as fc
from utils.mugics import Mugics
from utils.quickloop import QuickLoop

server = pyo.Server(audio='jack', ichnls=8, nchnls=4, jackname='multimugic')

server.setJackAuto(False, False)
server.setJackAutoConnectInputPorts([
    [f'system:capture_{i}'] for i in range(1,9)
])
server.setJackAutoConnectOutputPorts([
    [f'system:playback_{i}'] for i in range(1,5)
])

fc.init(server, text="MGIC", model=2, device_index=2)
server.stop()

# sound through

global_vol = pyo.OscReceive(9002, '/global_vol')

vols = pyo.OscReceive(9000, [f'/vol{i}' for i in range(8)])
ins = [pyo.Input(i, mul=vols[f'/vol{i}']) for i in range(8)]

mix = pyo.Mix(ins, mul=global_vol)
pan = pyo.Pan(mix).out()
send = pyo.Sig(pan).out(2)

# Quickloops

qls = [
    QuickLoop(ins[i], i+1, vol=global_vol*1.2) for i in range(8)
]

ql_send = pyo.Mix([ql.output for ql in qls]).mix(1).out(2)

mu2loops = [None] * 8

# Mugics

mugics = Mugics()

# OpenStageControls buttons & co

def handle_commands(_address, *args):

    command, *args = args
    if command == 'set_north':
        print('Setting north ;-)')
        mugics.set_EX_origins(180)
    elif command == 'loops':
        print('LOOPS:', args[0])
        if args[0]:
            for t in triggers:
                t.play()
        else:
            for t in triggers:
                t.stop()
    else:
        print('Unknown command:', command)

osc_commands = pyo.OscDataReceive(9001, "/commands", handle_commands)

def handle_connections(address, *args):

    try:
        m, l = args
    except ValueError:
        m = int(args[0][-1])
        l = None
    else:
        m = int(m[-1])
        l = int(l[-1])-1

    print(f'mu{m} --> l{l}')
    mu2loops[m-1] = l


osc_connects = pyo.OscDataReceive(9003, "/mu2loops/*", handle_connections)

# Record QLs with mugics

def mu2loop(n):
    l = mu2loops[n-1]
    if l is not None:
        return qls[l]

def rec(n):
    print('rec', n)
    l = mu2loop(n)
    if l:
        l.press()

def stop_rec(n):
    print('stop rec', n)
    l = mu2loop(n)
    if l:
        l.release()

def clear(n):
    print('clear', n)
    l = mu2loop(n)
    if l:
        l.stop()

exs = pyo.Sig([mugics[i+1].EX for i in range(8)])

rec_t = pyo.Thresh(exs, 240, dir=0).stop()
rec_tf = pyo.TrigFunc(rec_t, rec, arg=[1,2,3,4,5,6,7,8])

stoprec_t = pyo.Thresh(exs, 240, dir=1).stop()
stoprec_tf = pyo.TrigFunc(stoprec_t, stop_rec, arg=[1,2,3,4,5,6,7,8])

clear_t = pyo.Thresh(exs, 110, dir=1).stop()
clear_tf = pyo.TrigFunc(clear_t, clear, arg=[1,2,3,4,5,6,7,8])

triggers = [rec_t, stoprec_t, clear_t]

# Battery state

send_batt = pyo.OscDataSend(
    'if',
    address='/batt',
    port=8080,
)

send_ex = pyo.OscDataSend(
    'if',
    address='/ex',
    port=8080,
)


def sender():

    for i in range(1,9):
        send_batt.send([i,mugics[i].Battery.get()])
        send_ex.send([i, mugics[i].EX.get()])

battpat = pyo.Pattern(sender, time=.2).play()

server.start()

input('Press enter to exit.')
