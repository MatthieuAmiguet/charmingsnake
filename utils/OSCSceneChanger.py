import pyo
import context

class OSCSceneChanger:

    def __init__(self, port):

        context.SceneChanger = pyo.OscDataReceive(port, '/scene', self.change_scene)

    def change_scene(self, _addr, scene_name):
        context.change_scene(short_name=scene_name)
