''' 
MIDI-to-OSC bridge. Trying to make it pyo.Server-less.
'''

import sys
import pyo
from importlib import import_module

# Load setup file

try:
    setup_file = sys.argv[1]
except IndexError:
    print("Usage: %s <setup_file>" % sys.argv[0])
    input('Press ENTER to quit')
    sys.exit(0)

try:
    ps = import_module(setup_file)
except Exception as e:
    print('ERROR: failed to load setup file!')
    print(e)
    input('Press ENTER to quit')
    sys.exit(0)

# Poor man's calibration

calib_min = 2
calib_max = 125
calib_pow = .8

cache = ps.inits

def display_values():
    print("\033[H", end='') # move cursor to (0,0)
    print('LA PIEUVRE\n')
    for i, v in enumerate(cache):
        hashes = '#' * int(v*50)
        print(f'P{i+1}: {hashes:50} ({v:.2f})')

def midicall(status, data1, data2):
    ''' MIDI callback '''

    if not (176 <= status <= 179) or not data1 == 7:
        print('Warning: Unexpected MIDI Message!', status, data1, data2)
        return

    pedal = status - 176

    val = min(
        1,
        max(
            0,
            (data2-calib_min)/(calib_max-calib_min)
        )
    )**calib_pow

    ps.ped2sends[pedal](val)
    cache[pedal] = val
    display_values()

def init():
    ''' Creates the MIDI thread. Must be called before it works! '''

    global listen

    idev = dict(zip(*pyo.pm_get_input_devices()))['MIDI Expression BLACK MIDI 1']
    listen = pyo.MidiListener(midicall, idev)
    listen.start()

if __name__ == '__main__':

    init()
    for s, v in zip(ps.ped2sends, ps.inits):
        s(v)

    print("\033[2J", end='') # clear screen
    display_values()
    input()
