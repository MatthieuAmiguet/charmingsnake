'''
Version pragmatico-pragmatique de la gestion des mugics pour
"Nouveaux Rivages".
Rien de très beau ni très réutilisable, juste du pragmatique...
'''

from collections import deque
import pyo
import time

datarate = 1/40

# EX 0..360
# EY -90..90
# EZ -180..180
# Quaternions -1..1

datagram = (
    'AX', 'AY', 'AZ', # accelerometer
    'EX', 'EY', 'EZ', # Euler angles
    'GX', 'GY', 'GZ', # Gyrometer
    'MX', 'MY', 'MZ', # Magnetometer
    'QW', 'QX', 'QY', 'QZ', # Quaternions
    'Battery', 'mV', # Battery state
    'calibsys', 'calibgyro', 'calibaccel', 'calibmag', # Calibration state
    'seconds', # since last reboot
    'seqnum', # messagesequence number
)

value2pos = {
    val: pos
    for pos, val in enumerate(datagram)
}

class MovingSum:

    def __init__(self, window=40):

        self.init = [0]*window
        self.window = window
        self.history = deque(self.init, maxlen=window)
        self.sum = 0

    def append(self, value):

        self.sum -= self.history.popleft()
        self.sum += value
        self.history.append(value)
        return self.sum

    def value(self):

        return self.sum

    def clear(self):

        self.history.extend(self.init)
        self.sum = 0


class Mugics:

    def __init__(self, port=4000, number=6):

        self.listener = pyo.OscDataReceive(port, [f"/mugic/{i+1}" for i in range(number)], self.update_values)
        self.number = number
        self.moves = [MovingSum() for _ in range(6)]
        self.m5Euler = (pyo.Sig(0), pyo.Sig(0), pyo.Sig(0))
        self.callback = None
        self.skip = [0] * number

    def update_values(self, address, *args):

        n = int(address[-1]) - 1
        args = list(args)

        if n == 4: # We only need EX, EY, EZ for mugic #5
            for sig, val in zip(self.m5Euler, args[3:6]):
                sig.value = val

        gz = args[8]

        if self.skip[n]: # for debounce
            self.skip[n] -= 1
            self.moves[n].append(0)
            return

        move = self.moves[n].append(gz)

        if move >= 2400:
            print(n+1, ' gauche <--')
            # self.moves[n].clear()
            self.skip[n] = 40
            if self.callback:
                self.callback(n+1, -1)
        elif move <= -2400:
            print(n+1, ' droite -->')
            # self.moves[n].clear()
            self.skip[n] = 40
            if self.callback:
                self.callback(n+1, 1)

    def set_callback(self, f):
        self.callback = f

if __name__ == '__main__':

    s = pyo.Server().boot().start()
    mugics = Mugics()

    while True:
        time.sleep(1)

