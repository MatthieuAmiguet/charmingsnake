from enum import Enum
import pyo

# FIXME: this code should probably use pyo.Fader's instead of setMul's, but, as they say
# if it ain't broke don't fix it...

# FIXME: copied and modified from quickloop.py. A DRY approach would ask for a single, adaptable
# code base.

STATE = Enum('STATE', 'IDLE REC PLAY')


class QuickLoop:
    
    def __init__(self, input, vol=1, inner_xfade=20, outer_xfade=.5, channel=0, state_callback=None):

        self.channel = channel
        self.outer_xfade = outer_xfade

        self.tabs = [pyo.NewTable(30), pyo.NewTable(30)]
        self.tabrs = [pyo.TableRec(input, tab) for tab in self.tabs]
        sr = input.getSamplingRate()

        self.loops = [
            pyo.Looper(
                self.tabs[i],
                dur=self.tabrs[i]['time'] / sr,
                xfade=inner_xfade,
                mul=0
            )
            for i in [0,1]]
        
        self.voice = 0
        
        self.output = pyo.Selector(self.loops, voice=self.voice, mul=vol).stop()
        
        self.cb = state_callback
        self.state = STATE.IDLE

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, value):
        if self.cb:
            self.cb(value)
        self._state = value

    def rec(self):

        if self.state != STATE.REC:
            self.tabrs[1-self.voice].play()
            self.state = STATE.REC


    def playback(self):

        if self.state != STATE.REC:
            return

        self.voice = 1 - self.voice
        self.tabrs[self.voice].stop()
        self.loops[self.voice].reset()
        self.loops[self.voice].setMul(1)
        self.loops[self.voice].play()
        self.output.set('voice', self.voice, self.outer_xfade)
        self.output.out(self.channel, inc=0)
        self.state = STATE.PLAY

    def stop_output(self):
        
        self.output.stop()
        self.loops[self.voice].stop()

    
    def stop(self, wait=0):
        # wait arg is ignored!
        
        if self.state == STATE.IDLE:
            return

        self.loops[self.voice].set('mul', 0, .1, self.stop_output)
        self.state = STATE.IDLE

    def get_tab(self):
        return self.tabs[self.voice]

    def get_dur(self):
        return self.loops[self.voice].dur

if __name__ == '__main__':

    s = pyo.Server(audio='jack', nchnls=1).boot().start()

    def dump(state):
        print(f'State: {state.name}')

    i = pyo.Input(0)
    ql = QuickLoop(i, state_callback=dump)
    
    while ui := input('Commande (r/p/s): '):

        if ui == 'r':
            ql.rec()
        elif ui == 'p':
            ql.playback()
        elif ui == 's':
            ql.stop()
