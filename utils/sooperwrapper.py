import pyo
from foococo import foococo as fc
import context

context.sl_feedback = False

class SLFeedback:

    def __init__(self, l1=1, l2=0, inport=9901):
        ''' l1 is on leds 1-4, l2 on leds 6-9 '''

        self.loops = [l1, l2]
        self.l1 = l1
        self.l2 = l2
        self.fc = fc
        self.states = {l: 0 for l in self.loops}

        self.register = pyo.OscDataSend(
            'siss', 
            9951, 
            [ f'/sl/{i}/register_auto_update' for i in self.loops]
        )
        self.register.send(['state',100,f'localhost:{inport}', '/loop_state'])

        self.get_state = pyo.OscDataReceive(inport, '/loop_state', self.loop_state)


    def loop_state(self, _address, *args):

        loop_index, control, value = args

        if control != u'state':
            print("Unknown control: %s" % control)
            return

        self.states[loop_index] = value

        if not context.sl_feedback:
            return

        base = 1 if loop_index==self.l1 else 6

        if value == 0:
            self.fc.led_off([i+base for i in range(4)])()

        elif value == 1: # wait start
            self.fc.blink(base, 'red')()

        elif value == 2: # recording
            self.fc.led_on(base, 'red')()

        elif value == 3: # wait stop
            self.fc.blink(base, 'green')()

        elif value == 4: # playing
            self.fc.led_on(base, 'green')()
            self.fc.led_off([base+1, base+2])()
            self.fc.led_on(base+3, 'green')()

        elif value == 5: # overdubbing
            self.fc.led_on(base, 'green')()
            self.fc.led_on(base + 1, 'red')()

        elif value == 10: # muted
            self.fc.led_on(base+3, 'red')()

        elif value == 20: # offMuted
            self.fc.led_off([i+base for i in range(3)])()
            self.fc.led_on(base+3, 'red')()


        else:
            print("Unhandled message: %s, %s, %s" % (loop_index, control, value))


    def resync_leds(self):

        for i, s in self.states.items():
            self.loop_state(None, i, 'state', s)

class SLCommand:

    def __init__(self, l1=1, l2=0, *, start=None):

        self.loop = [
            pyo.OscDataSend('s', 9951, f'/sl/{l2}/hit'),
            pyo.OscDataSend('s', 9951, f'/sl/{l1}/hit')
        ]

        self.lower_row = [
            fc.Press(fc.button(1), lambda: self.loop[1].send(['record'])),
            fc.Press(fc.button(2), lambda: self.loop[1].send(['overdub'])),
            fc.Press(fc.button(3), lambda: self.loop[1].send(['undo'])),
            fc.Press(fc.button(4), lambda: self.loop[1].send(['mute'])),
        ]

        self.upper_row = [
            fc.Press(fc.button(6), lambda: self.loop[0].send(['record'])),
            fc.Press(fc.button(7), lambda: self.loop[0].send(['overdub'])),
            fc.Press(fc.button(8), lambda: self.loop[0].send(['undo'])),
            fc.Press(fc.button(9), lambda: self.loop[0].send(['mute'])),
        ]

        self.undo_all = fc.Press(fc.button('nav_down'), [
            lambda: self.loop[0].send(['undo_all']),
            lambda: self.loop[1].send(['undo_all']),
        ])

        self.global_vol = pyo.OscDataSend('sf', 9951, '/sl/-1/set')

        if start is not None: # start is the autoplayer

            for o in self.lower_row + self.upper_row + [self.undo_all]:
                start.play(o)
