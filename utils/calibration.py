import sys
from pathlib import Path
import time
import pyo
from foococo import foococo as fc

config_dir = Path.home() / '.ss_calibrations'

def update():
    fc.calibrate_pedal(e_min, e_max, e_curve)


def set_min():
    global e_min
    e_min = pedal.raw.get() # get uncalibrated value
    update()


def set_max():
    global e_max
    e_max = pedal.raw.get() # get uncalibrated value
    update()


def set_curve(val):
    global e_curve
    e_curve = val/100
    update()


def save(index, model):

    if not config_dir.exists():
        print(f'creating path: {config_dir}')
        config_dir.mkdir()

    config_file = config_dir / f'calib_{index}_{model}.txt'
    with open(config_file, mode='w') as f:
        s = f'{e_min:.3f} {e_max:.3f} {e_curve}'
        print(f'Writing calibation values into {config_file}:')
        print(s)
        f.write(s)


def load(index, model):
    global e_min
    global e_max
    global e_curve

    config_file = config_dir / f'calib_{index}_{model}.txt'
    try:
        with open(config_file) as f:
            e_min, e_max, e_curve = [float(v) for v in f.read().strip('\n').split()]
        print(f'Pedal calibration found: {e_min} {e_max} {e_curve}')
    except Exception as e:
        print(f'Could not read calibration file: {e}')
        print('Using default values.')
        e_min = 0
        e_max = 1
        e_curve = .8

    update()


def finished():
    global running
    server.stop()
    save(index, model)
    running = False

if __name__ == '__main__':

    try:
        index = int(sys.argv[1])
        model = int(sys.argv[2])
    except:
        print('Usage: Calibrate <device_index> <model>')
        sys.exit(1)

    server = pyo.Server(
        audio='jack',
        nchnls=0,
        jackname='calibrate'
    )


    fc.init(server, text="CALI", model=model, device_index=index)
    server.stop()

    pedal = fc.extension_pedal()

    load(index, model)

    disp = fc.Pressure(pedal, fc.display('Expr'))

    b1 = fc.Press(
            fc.button(1),
            [
                fc.display('** 0'),
                fc.flash(1),
                set_min
            ]
    )

    b6 = fc.Press(
            fc.button(6),
            [
                fc.display('*100'),
                fc.flash(6),
                set_max
            ]
    )

    curve = fc.Expression(
        fc.button(7),
        fc.button(2),
        [
            fc.display('CURV'),
            set_curve,
        ],
        init=e_curve,
    )


    running = True

    b0 = fc.Press(
            fc.button(0),
            [
                fc.display('BYE'),
                finished
            ]
    )

    server.start()

    while running:
        time.sleep(.1)
