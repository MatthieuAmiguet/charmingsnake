import pyo
from foococo import foococo as fc
from time import time

# FIXME: this code should probably use pyo.Fader's instead of setMul's, but, as they say
# if it ain't broke don't fix it...

class QuickLoop:
    
    def __init__(self, input, place, vol=1, inner_xfade=20, outer_xfade=.5, channel=0, listen=None, min_dur=.15):

        self.channel = channel
        self.outer_xfade = outer_xfade
        self.min_dur = min_dur

        self.tabs = [pyo.NewTable(30), pyo.NewTable(30)]
        self.tabrs = [pyo.TableRec(input, tab) for tab in self.tabs]
        sr = input.getSamplingRate()

        self.loops = [
            pyo.Looper(
                self.tabs[i],
                dur=self.tabrs[i]['time'] / sr,
                xfade=inner_xfade,
                mul=0
            )
            for i in [0,1]]
        
        self.voice = 0
        
        self.output = pyo.Selector(self.loops, voice=self.voice, mul=vol).stop()
        
        self.state = 0
        self.place = place
        
        self.button = fc.button(place)
        
        self.bs = [
            fc.Press(self.button, self.press),
            fc.Release(self.button, self.release)
        ]

        self.last_press = time()

        # listen is an autoplayer: so this schedules self.bs start/stop
        # on calling tune's start/stop
        if listen is not None:
            for b in self.bs:
                listen.play(b)


    def listen(self):

        for b in self.bs:
            b.play()

    
    def ignore(self):
    
        for b in self.bs:
            b.stop()


    def press(self):

        self.last_press = time()

        # when pressing, record in bg
        self.tabrs[1-self.voice].play()
        fc.led_on(self.place, 'red')()
        
    
    def release(self):
        
        dur = time()-self.last_press
        
        if self.state == 0 or dur > self.min_dur: # was idle or "long" press, go to play
            self.voice = 1 - self.voice
            self.tabrs[self.voice].stop()
            self.loops[self.voice].reset()
            self.loops[self.voice].setMul(1)
            self.loops[self.voice].play()
            self.output.set('voice', self.voice, self.outer_xfade)
            self.output.out(self.channel, inc=0)
            fc.led_on(self.place, 'green')()
            self.state = 1 # playing

        else: # short press when playing, go to idle
            self.loops[self.voice].set('mul', 0, self.outer_xfade) #, self.stop_output)
            fc.led_off(self.place)()
            self.state = 0 # idle
        
    def stop_output(self):
        
        self.output.stop()
        self.loops[self.voice].stop()

    
    def stop(self, wait=0):
        # wait arg is ignored!
        
        self.loops[self.voice].set('mul', 0, .1, self.stop_output)
        fc.led_off(self.place)()
        self.state = 0

    def get_tab(self):
        return self.tabs[self.voice]

    def get_dur(self):
        return self.loops[self.voice].dur

    def resync_led(self):
        
        if self.state == 0:
            fc.led_off(self.place)()
        
        elif self.state == 1:
            fc.led_on(self.place, 'green')()

