function wait_for_jack_port () {
    while ! jack_lsp 2>/dev/null | grep "$1" >/dev/null; do sleep .1; done
}

function wait_no_jack_port () {
    while jack_lsp 2>/dev/null | grep "$1" >/dev/null; do sleep .1; done
}

function terminate () {
    echo "--> " $(ps -p $1 -o args=)
    kill $1
    echo killed.
}

function cleanup_jobs () {
    echo ---------------------------------------------
    echo "Killing jobs in reverse launch order"
    for j in $( jobs -p | tac | head -n -1 ); do
        terminate $j
    done
    echo Waiting...
    echo "Killing remaining job"
    sleep .5
    terminate $(jobs -p %1)
    echo done.
    echo ---------------------------------------------
    sleep .5
}
