import pyo
import nk2
import time
import os
import sys

# For Traces de Souffle:
# Connect alternate output of main mixer
# to input 6 of sound card
# (this allows to capture external reverb)

# ======================================
# Initialization
# ======================================

s = pyo.Server(audio='jack', nchnls=1)
s.setJackAuto(False, False)
s.setJackAutoConnectInputPorts([
    ['system:capture_6']
])
s.setJackAutoConnectOutputPorts([
    ['system:playback_1']
])
nk2.init(s)
s.boot()

s.start()
nk2.leds_external()

# ======================================
# Recorders
# ======================================

class Recorder():
    
    def __init__(self, track, duration=20):
        
        self.track = track
        self.duration = duration
        self.table = pyo.NewTable(duration)
        self.rec_button = pyo.TrigFunc(nk2.Press('r%d' % track), self.start_recording)
        self.tablerec = pyo.TableRec(pyo.Input(), self.table).stop()
        self.stoptrig = self.tablerec['trig']
        self.stoprec = pyo.TrigFunc(self.stoptrig, self.stop_recording)
        self.looper = pyo.Looper(
            self.table,
            dur= duration,
            xfade = 10,
            mul=nk2.fader(track)
        ).stop()
        
    def start_recording(self):
        
        nk2.led_on('r%d' % self.track)
        self.tablerec.play()
        
    def stop_recording(self):
        
        nk2.led_off('r%d' % self.track)
        nk2.led_on('m%d' % self.track)
    
    def out(self):
        
        nk2.led_off('m%d' % self.track)
        nk2.led_on('s%d' % self.track)
        self.looper.out()
    
recorders = [Recorder(i) for i in range(8)]

# ======================================
# The big picture
# ======================================

def play():
    
    for r in recorders:
         r.out()

theBigPlayButton = pyo.TrigFunc(nk2.Press('play'), play)

        

# ======================================
# Life cycle management
# ======================================

alive = pyo.Pattern(lambda: nk2.led_blink('cycle'), 2).play()

def restart():
    global stopped
    stopped = True
    
restart_tf = pyo.TrigFunc(nk2.Press('cycle'), restart)

stopped = False
while not stopped:
    time.sleep(1)

# hack: reset all leds
nk2.leds_external()

# stop server to quit gracefully
s.stop()
