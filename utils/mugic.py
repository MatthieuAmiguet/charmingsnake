import pyo
import time
import math


datarate = 1/40

# EX 0..360
# EY -90..90
# EZ -180..180
# Quaternions -1..1

datagram = (
    'AX', 'AY', 'AZ', # accelerometer
    'EX', 'EY', 'EZ', # Euler angles
    'GX', 'GY', 'GZ', # Gyrometer
    'MX', 'MY', 'MZ', # Magnetometer
    'QW', 'QX', 'QY', 'QZ', # Quaternions
    'Battery', 'mV', # Battery state
    'calibsys', 'calibgyro', 'calibaccel', 'calibmag', # Calibration state
    'seconds', # since last reboot
    'seqnum', # messagesequence number
)

class Mugic:

    def __init__(self, port=4000, alive_callback=None):

        self.values = {}
        self.listener = pyo.OscDataReceive(port, "/mugic/1", self.update_values)
        # self.a = pyo.Sig(0)
        # self.tt = pyo.Follower2(self.a, falltime=4)
        self.ex_offset = 0
        self.raw_ex = 0
        self.counter = 0
        self.alive_cb = alive_callback

    def update_values(self, _address, *args):

        if self.alive_cb:
            self.counter += 1
            if self.counter == 80:
                self.alive_cb()
                self.counter = 0

        # EX offset (aka "set north")
        args = list(args)
        args[3] = (args[3]-self.ex_offset) % 360
        self.raw_ex = args[3]

        for k, v in zip(datagram, args):

            try:
                self.values[k].value = v
            except KeyError:
                pass

        # self.a.value = math.sqrt(self.AX.get()**2 + self.AY.get()**2 + self.AZ.get()**2)

    def set_EX_origin(self):

        self.ex_offset = self.raw_ex

    def __getattr__(self, k):

        if k not in datagram:
            raise AttributeError()

        try:
            return self.values[k]
        except KeyError:
            pass

        st = pyo.SigTo(0, datarate)
        # FIXME: allows to avoid xruns on mugic scene start/stop, but...
        st.allowAutoStart(False)
        self.values[k] = st
        return st



if __name__ == '__main__':

    s = pyo.Server().boot().start()
    mugic = Mugic()

    noise = pyo.Noise()
    f = pyo.Reson(
        noise, 
        [
            (mugic.AX + 10)*100,
            (mugic.AY + 5)*100,
            (mugic.AZ + 7)*100,
        ],
        [v+180 for v in [mugic.EX,mugic.EY,mugic.EZ]]
    ).out()

    while True:

        print("\033[H\033[J", end='') # clear screen
        print("\033[H", end='') # Ansi code for move cursor to (0,0)... portable?
        for k in datagram:

            try:
                print(f'{k:8}: {getattr(mugic, k).get():8.2f}')
            except KeyError:
                pass

        # print('a: ', '##' * (p:=int(mugic.a.get())), p)
        # print('tt: ', '##' * (p:=int(mugic.tt.get())), p)

        time.sleep(datarate)
