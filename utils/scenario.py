from numbers import Number
import pyo
from foococo import foococo as fc

class ResetScenario(Exception):
    pass


class Pool:

    def get_next_available(self):

        try:
            o = self.objs[self.index]
        except IndexError:
            raise IndexError('Not enough TrigFuncs. Please increase max number at object creation.')

        self.index += 1
        return o

    def stop_all(self):

        for o in self.objs[:self.index]:
            o.stop()

        self.index = 0

    def suspend(self, d=0):

        for o in self.objs[:self.index]:
            o.stop(d)

    def resume(self):

        for o in self.objs[:self.index]:
            o.play()


class TrigFuncPool(Pool):

    def __init__(self, func, size):

        # I just need a placeholder for creating my TrigFunc
        self._dummy = pyo.Sig(0).stop()
        self._dummy.allowAutoStart(False)

        self.objs = [pyo.TrigFunc(self._dummy, func).stop() for _ in range(size)]
        # The TrigFunc's must be used in reverse order from their creation.
        # See https://groups.google.com/g/pyo-discuss/c/bKaw8T9VAIw/m/rXMfP_oEBgAJ for details
        self.objs.reverse()
        self.index = 0

    def start_new(self, trig, arg):

        o = self.get_next_available()
        o.setInput(trig)
        o.setArg(arg)
        o.play()

    # def stop_all(self):

        # # It's not enough to stop the tfs, we need to change their input
        # # whether it's a bug or not in pyo is not clear at the time of
        # # writing this code...

        # for o in self.objs[:self.index]:
            # o.setInput(self._dummy)
            # o.stop()

        # self.index = 0


class CallAfterPool(Pool):

    def __init__(self, func, size):

        self.objs = [pyo.CallAfter(func, 1).stop() for _ in range(size)]
        self.index = 0

    def start_new(self, time, arg):

        o = self.get_next_available()
        o.setTime(time)
        o.setArg(arg)
        o.play()


class Scenario:

    # Max number of TrigFunc's and CallAfter's. 
    # Override in subclasses if needed.
    MAX_TF = 3
    MAX_CA = 1

    def __init__(self, max_tf=3):

        self.tfs = TrigFuncPool(self.step, self.MAX_TF)
        self.cas = CallAfterPool(self.step, self.MAX_CA)

        self.setup(initial=True)
        self._state = self.runner()
        self.step(None)


    def play(self):

        print('Scenario play')
        self.tfs.resume()
        self.cas.resume()

    def stop(self, d=0):

        print('Scenario stop')
        self.tfs.suspend(d)
        self.cas.suspend(d)

    def step(self, triggering_event_index=0):

        self.tfs.stop_all()
        self.cas.stop_all()

        wait_for = self._state.send(triggering_event_index)

        if wait_for is None: return

        if not isinstance(wait_for, tuple):
            wait_for = (wait_for,)

        for i, event in enumerate(wait_for):

            match event:

                case Number(): # wait for i seconds
                    self.cas.start_new(event, i)
                case fc.Press(): # wait for SoftStep Button Press
                    self.tfs.start_new(event.trig, i)
                case pyo.PyoObject(): # wait for generic pyo Trigger
                    self.tfs.start_new(event, i)
                case _:
                    print('Unknown transition type!')


    def setup(self, initial):
        raise NotImplementedError

    def steps(self):
        raise NotImplementedError

    def runner(self):
        ''' Wrapper around self.steps to manage resets and scenario ending.
        Without it we would have to handle it in the subclass' steps().'''

        while 1:
            try:
                yield from self.steps()
                print('Scenario is over.')
                yield # keep scenario "alive" to be able to restart it
            except ResetScenario:
                self.setup(initial=False)
                self._state = self.runner()
                self.step(None)

    def restart(self):

        self._state.throw(ResetScenario())


if __name__ == '__main__':

    s = pyo.Server().boot().start()

    # fc.init(s, 'SCEN')

    def display(t):
        print(t)

    b1 = pyo.Trig()
    b2 = pyo.Trig()

    class MyScenario(Scenario):

        def setup(self, initial):

            if initial:
                print('Setting things up.')
            else:
                print('Resetting scenario.')

            self.state = 1

        def steps(self):

            while 1:

                match self.state:

                    case 1:

                        display('S1')
                        match (yield b1, b2):

                            case 0: self.state = 2
                            case 1: self.state = 3

                    case 2:

                        display('S2')
                        match (yield b1, 2):

                            case 0: self.state = 4
                            case 1: self.state = 3

                    case 3:

                        display('S3')
                        match (yield b1, b2):

                            case 0: self.state = 4
                            case 1: self.state = 1


                    case 4:

                        display('S4')
                        yield b2
                        self.state = 3


    scen = MyScenario()

    b3 = fc.Press(fc.button(3), scen.restart)

