import pyo
import time

datarate = 1/40

# EX 0..360
# EY -90..90
# EZ -180..180
# Quaternions -1..1

datagram = (
    'AX', 'AY', 'AZ', # accelerometer
    'EX', 'EY', 'EZ', # Euler angles
    'GX', 'GY', 'GZ', # Gyrometer
    'MX', 'MY', 'MZ', # Magnetometer
    'QW', 'QX', 'QY', 'QZ', # Quaternions
    'Battery', 'mV', # Battery state
    'calibsys', 'calibgyro', 'calibaccel', 'calibmag', # Calibration state
    'seconds', # since last reboot
    'seqnum', # messagesequence number
)

class _Mugic:

    def __init__(self, mugics, n):

        self.mugics = mugics
        self.n = n


    def __getattr__(self, k):

        if k not in datagram:
            raise AttributeError()

        try:
            return self.mugics.values[self.n][k]
        except KeyError:
            pass

        st = pyo.SigTo(0, datarate)
        # FIXME: allows to avoid xruns on mugic scene start/stop, but...
        st.allowAutoStart(False)
        self.mugics.values[self.n][k] = st
        return st

class Mugics:

    def __init__(self, port=4000, number=8):

        self.values = [{} for i in range(number)]
        self.listener = pyo.OscDataReceive(port, [f"/mugic/{i+1}" for i in range(number)], self.update_values)
        self.ex_offset = [0] * number
        self.raw_ex = [0] * number
        self.counter = [0] * number
        self.number = number

    def __getitem__(self, key):

        return _Mugic(self, key-1)

    def update_values(self, address, *args):

        n = int(address[-1]) - 1
        # EX offset (aka "set north")
        args = list(args)
        self.raw_ex[n] = args[3]
        args[3] = (args[3]-self.ex_offset[n]) % 360

        for k, v in zip(datagram, args):

            try:
                self.values[n][k].value = v
            except KeyError:
                pass


    def set_EX_origins(self, force_current=0):

        for i in range(self.number):
            self.ex_offset[i] = (self.raw_ex[i] - force_current) % 360




if __name__ == '__main__':

    s = pyo.Server().boot().start()
    mugics = Mugics()

    while True:

        print("\033[H\033[J", end='') # clear screen
        print("\033[H", end='') # Ansi code for move cursor to (0,0)... portable?
        for i in range(8):
            m = mugics[i+1]
            print(f'Mugic #{i+1} EX: {m.EX.get():8.2f}, {m.Battery.get():3}%, {m.seqnum.get():10.0f}')

        time.sleep(datarate)
