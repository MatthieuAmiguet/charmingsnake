from pyo import *
import context as ctx

class LiveLooper(PyoObject):
    
    def __init__(self, input, max_dur=60, xfade=0, play_sync=None, mul=1, add=0, pitch=1, fadetime=0, name=''):

        PyoObject.__init__(self, mul, add)
        
        self.name = name
        self.tab = NewTable(max_dur)
        self.trec = TableRec(input, self.tab, fadetime=fadetime)
        self.looper = Looper(
            self.tab,
            dur=self.trec['time'] / ctx.server.getSamplingRate(),
            xfade = xfade,
            pitch=pitch,
            mul=mul,
            add=add
        ).stop()
        
        self.looper.fadeInSeconds(1)
        
        if play_sync is not None:
            self.psync = True
            self.psync_source = play_sync.on_loop_start()
            self.psync_cb = TrigFunc(self.psync_source, self.check_psync).stop()
        else:
            self.psync = False
            
        self._base_objs = self.looper.getBaseObjects()
        
    def rec(self):
        
        self.trec.play()
    
    def stop_rec(self):
        
        self.trec.stop()
        
    def play(self, dur=0, delay=0):

        if self.psync:
            self.psync_cb.play()

        self.looper.play()
        
        return PyoObject.play(self, dur, delay)

    def out(self, chnl=0, inc=1, dur=0, delay=0):
        
        if self.psync:
            self.psync_cb.play()
        
        self.looper.play() # WHY DO I NEED THIS?
        
        return PyoObject.out(self, chnl, inc, dur, delay)
        
    def stop(self, d=0):
        
        if self.psync:
            self.psync_cb.stop(d)

        self.looper.stop(d)

        return PyoObject.stop(self, d)

    def on_loop_start(self):
        
        return self.looper['trig']

    def check_psync(self):
        
        dur = self.looper.dur.get()
        pos = self.looper['time'].get() * dur
        if pos < 0.5 or (dur-pos < 0.5):
            self.looper.loopnow()
