# coding: utf-8

import pyo
from foococo import foococo as fc
import context

class VarDelay:
    
    def __init__(self, input, place, reset=None, number=2, max_length=2, margin=3, channel=0, inc=0, listen=None):

        # margin évite qu'on se "morde la queue" si on accélère trop...
        # (même avec le double-buffering, il se peut qu'on ré-enregistre sur un buffer
        # en cours de lecture...)

        # listen is the calling tune's autoplayer

        self.channel = channel
        self.inc = inc

        self.tabs = [pyo.NewTable(max_length) for _ in range(number+margin)]
        
        self.switch = listen.play(
            pyo.Switch(input)
        )
        self.recs = [pyo.TableRec(self.switch[i], self.tabs[0]) for i in range(2)]

        self.players = [pyo.TableRead(self.tabs[:-1], freq=1.0/max_length) for _ in range(2)]
        self.output = listen.out(
            pyo.Selector([p.mix(1) for p in self.players]),
            channel,
            inc
        )
        
        self.current = 0
        self.number = number
        self.margin = margin
        
        self.buffer = 0
        
        self.b = listen.play(
            fc.Press(fc.button(place), self.press)
        )

        if reset is not None:
            self.br = listen.play(
                fc.Press(fc.button(reset), self.reset)
            )



    def press(self):

        self.buffer = 1-self.buffer

        self.players[self.buffer].stop()        
        self.players[self.buffer].setTable([self.tabs[self.current-i] for i in range(self.number)])
        self.players[self.buffer].play()
        self.output.set('voice', self.buffer, .1)
        
        self.current = (self.current + 1) % (self.number + self.margin)
        
        self.recs[self.buffer].stop()
        self.tabs[self.current].reset()
        self.recs[self.buffer].setTable(self.tabs[self.current])
        self.recs[self.buffer].play()
        self.switch.set('voice', self.buffer, .1)
        
    def reset(self):

        context.call_asap(
            *[t.reset for t in self.tabs]
        )
