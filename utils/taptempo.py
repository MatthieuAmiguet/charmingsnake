from collections import deque
from time import time
import pyo
from foococo import foococo as fc

# Loosely inspired by https://rdsquared.wordpress.com/2011/06/28/creating-a-tap-tempo-feature/
# with the additional idea of "resistance to aberration"

class TapTempo:
    
    def __init__(self, place, init=1, window_size=5, port_time=.2, timeout=2, listen=None):

        # listen is the calling tune's autoplayer

        self.last_press = 0
        
        self.timeout = timeout
        self.window_size = window_size
        
        self.times = deque([init] * window_size, maxlen=window_size)
        self.average = init
        
        self.b = listen.play(
            fc.Press(fc.button(place), self.press)
        )
        self.value = listen.play(
            pyo.SigTo(init, port_time, init)
        )
        
        self.aberration = None

    def set(self, value):
        
        for _ in range(self.window_size):
            self.times.append(value)
        
        self.average = value
        self.value.setValue(value)

    def press(self):

        now = time()
        elapsed = now - self.last_press
        self.last_press = now
        
        if elapsed > self.timeout:
            return
        
        if not (self.average * .9 < elapsed < self.average * 1.1):
            
            # Tempo changed?
            if self.aberration is None:
                self.aberration = elapsed
            # Yes
            elif self.aberration * .9 < elapsed < self.aberration * 1.1:
                self.times.clear()
                self.times.append(self.aberration)
                self.times.append(elapsed)
            # No
            else:
                self.aberration = None
        
        self.times.append(elapsed)
        self.average = sum(self.times) / len(self.times)
        self.value.setValue(self.average)
        
    def freeze(self):
        
        self.b.stop()
    
    def unfreeze(self):
        
        self.b.play()
