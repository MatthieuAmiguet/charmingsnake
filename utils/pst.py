import pyo
from foococo import foococo as fc

class PST: # pitch shifting texture
    
    def __init__(self, input, place, length=1, vol=1, channel=0, pitch=1, fadein=.1, fadeout=.1, listen = None):

        self.place = place
        self.channel = channel
        self.length = length
        self.fadein = fadein
        self.fadeout = fadeout

        self.tab = pyo.NewTable(length)
        self.tabr = pyo.TableRec(input, self.tab)
        
        self.output = pyo.Looper(
            self.tab,
            dur=length,
            mul=pyo.Fader(fadein, fadeout, mul=vol),
            pitch=pitch,
        )
        self.output.setStopDelay(fadeout)
        
        self.state = 0
        self.delayed_led = pyo.CallAfter(fc.led_on(self.place, 'green'), self.length).stop()
        
        # listen is the calling tune's autoplayer
        self.b = listen.play(
            fc.Press(fc.button(place), self.press)
            )
        
    def press(self):
                
        if self.state == 0: #idle
            self.tabr.play()
            fc.led_on(self.place, 'red')()
            self.delayed_led.play()
            
            self.output.reset()
            self.output.out(self.channel, inc=0, delay = .1)
            
            self.state = 1
                
        elif self.state == 1: # recording and/or playing
                    
            self.stop()
            
    
    def stop(self, delay=0):
        
        self.delayed_led.stop()
        self.output.stop(delay)
        fc.led_off(self.place)()

        self.state = 0
        
