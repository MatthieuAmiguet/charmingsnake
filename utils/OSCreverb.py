import pyo

rev_table = {
    'bypass': 1,
    's': 2,
    'm': 3,
    'l': 4,
}

# MIDI PC 0-98 correspond to preset 1-99
offset = -1

last_sent = [-1, -1]

def init(s):

    idev = dict(zip(*pyo.pm_get_input_devices()))['AudioBox 1818 VSL MIDI 1']
    odev = dict(zip(*pyo.pm_get_output_devices()))['AudioBox 1818 VSL MIDI 1']
    server.setMidiInputDevice(idev)
    server.setMidiOutputDevice(odev)

def setReverb(address, type):

    program = rev_table[type]

    channel = 1 if address == '/reverb/1' else 2

    if last_sent[channel-1] == program:
        print("Reverb %d already active: %s" % (channel, type))
        return

    print("Setting reverb #%d to %s... " % (channel, type), end='')
    server.programout(program + offset, channel)
    print("done.")

    last_sent[channel-1] = program


server = pyo.Server(audio='jack', nchnls=0)
init(server)
server.boot()

receive = pyo.OscDataReceive(3456, ['/reverb/1', '/reverb/2'], setReverb)

server.start()

# tell both fx paths to go to "user" bank
server.ctlout(32, 1, channel=0)

import time
while True:
    time.sleep(1)
