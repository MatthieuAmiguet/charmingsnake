import pyo
from foococo import foococo as fc
import context
from autoplayer import Autoplayer

auto = Autoplayer()

short_name = 'KNON'
scrolling = auto.play(fc.Scroller("CANON"))

env = auto.play(pyo.SigTo(0, time=.1, mul=context.pedal))

b1 = fc.MultiState(
    fc.button(1),
    [
        [fc.led_off(1), lambda: env.setValue(0)],
        [fc.led_on(1, 'red')],
        [fc.led_on(1, 'green'), lambda: env.setValue(1) ],
    ],
    init=None
)
auto.play(b1)

t_b1 = b1.trig
ti = auto.play(pyo.Timer(t_b1,t_b1))

d = auto.out(
    pyo.SDelay(context.ql_send, delay=ti, maxdelay=60, mul=env)
)
