# TODO: migrate!

from foococo import foococo as fc
import pyo
from utils.looping import LiveLooper
import context
from autoplayer import Autoplayer

auto = Autoplayer()

# Display
short_name = 'CORL'
scrolling = auto.play(fc.Scroller("CHORAL"))


# Loopers
pass1 = LiveLooper(context.ql_send, max_dur=90)
pass2 = LiveLooper(context.ql_send, max_dur=90, play_sync=pass1)

bar = 0

def reset():
    
    global bar
    bar = 0
    
    for l in [pass1, pass2]:
        l.stop()
        l.looper.reset()
    
    context.mic.setVoice(.1)
    

b1 = fc.MultiState(fc.button(1),
    [
        [fc.led_on(1, 'green'), reset],
        [fc.led_on(1, 'red'), pass1.rec],
        [pass1.stop_rec, pass1.out, pass2.rec],
    ], init = None
)
auto.play(b1)


# Song structure
def update():
    global bar
    
    bar += 1.0
    
    if bar == 1:
        pass2.stop_rec()
        pass2.out()
        context.mic.setVoice(1)

    if bar == 2:
        
        b1.activate(0)


tf = auto.play(pyo.TrigFunc(pass1.on_loop_start(), update))


@auto.call_on_start
def start():
    for b in context.b5:
        b.stop()
    context.mic.setVoice(.1)

@auto.call_on_stop
def stop():

    for b in context.b5:
        b.play()
