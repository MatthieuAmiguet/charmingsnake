from foococo import foococo as fc
import context
from autoplayer import Autoplayer

auto = Autoplayer()

short_name = 'SL'
scrolling = auto.play(fc.Scroller("SOOPERLOOPER"))

# fc controls -> SooperLooper
from ..common.sooperlooper import loop, lower_row, upper_row, undo_all

undo_all_global = fc.Press(fc.button('nav_up'), [
    lambda: loop[0].send(['undo_all']),
    lambda: loop[1].send(['undo_all']),
])

for o in lower_row + upper_row + [undo_all]:
    auto.play(o)

# Explicit start/stop

@auto.call_on_start
def start():

    context.sl_feedback = True
    context.resync_sl_leds()


@auto.call_on_stop
def stop():

    context.sl_feedback = False
