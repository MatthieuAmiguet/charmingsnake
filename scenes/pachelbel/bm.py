import pyo
from foococo import foococo as fc
from utils.looping import LiveLooper
import context
from autoplayer import Autoplayer

auto = Autoplayer()

short_name = 'PACH'

# metronome

tempo = 120
dur = 60.0 / tempo

metro = pyo.Pattern(fc.flash(6), dur).stop()

# pedal

pedal = context.pedal

# Loops & delays

bass = LiveLooper(context.ql_send, mul=pedal*1.8, pitch=.5)

silence = auto.play(pyo.Sig(0))

d1 = pyo.SDelay(silence, maxdelay=30, mul=pedal).stop()
d2 = pyo.SDelay(silence, maxdelay=60, mul=pedal).stop()


def start_delays():
    
    dur=2*bass.looper.dur.get()
    
    d1.setDelay(dur)
    d2.setDelay(2*dur)
    
    for d in [d1, d2]:
        
        d.reset()
        d.setInput(context.ql_send)
        d.out()


b1 = fc.MultiState(
        fc.button(1),
        [
            [
                fc.led_off(1),
                bass.stop,
                bass.looper.reset,
                d1.stop,
                d2.stop,
                metro.play,
            ],
            [
                fc.led_on(1, 'red'),
                bass.rec,
            ],
            [
                fc.led_off(1),
                fc.blink(1, 'green', fast=True),
                bass.stop_rec,
                metro.stop,
            ],
            [
                fc.led_on(1, 'green'),
                bass.out,
                start_delays,
            ],
        ],
        init=None
    )
auto.play(b1)

# reset

b2 = fc.Press(
    fc.button(2),
    lambda: b1.activate(0)
)
auto.play(b2)

@auto.call_on_stop
def stop():
    b1.activate(0)

# The call to stop() has the side-effect to call metro.play()
# which will be executed later by foococo's scheduler.
# By registering a second function, we ensure that it will
# be run after all actions of stop() have taken place.
auto.call_on_stop(metro.stop)
