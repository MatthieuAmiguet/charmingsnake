# TODO: migrate!

from utils.autoplay import pyo
from utils.autoplay import foococo as fc
from utils.looping import LiveLooper
import context

short_name = 'PACH'

# metronome

tempo = 138
dur = 60.0 / tempo

metro = pyo.Pattern(fc.flash(6), dur).autoplay()

# pedal

pedal = pyo.SigTo(fc.extension_pedal(), .1).autoplay()

# Loops & delays

bass = LiveLooper(context.mic, mul=pedal*1.8)

silence = pyo.Sig(0).autoplay()

d1 = pyo.SDelay(silence, maxdelay=30, mul=pedal)
d2 = pyo.SDelay(silence, maxdelay=60, mul=pedal)


def start_delays():
    
    dur=bass.looper.dur.get()
    
    d1.setDelay(dur)
    d2.setDelay(2*dur)
    
    for d in [d1, d2]:
        
        d.reset()
        d.setInput(context.mic)
        d.out()


b1 = fc.MultiState(
        fc.button(1),
        [
            [
                fc.led_off(1),
                bass.stop,
                bass.looper.reset,
                d1.stop,
                d2.stop,
                metro.play,
            ],
            [
                fc.led_on(1, 'red'),
                bass.rec,
            ],
            [
                fc.led_off(1),
                bass.stop_rec,
                metro.stop,
                fc.led_on(1, 'green'),
                bass.out,
                start_delays,
            ],
        ],
        init=None
    ).autoplay()

# reset

b2 = fc.Press(
    fc.button(2),
    lambda: b1.activate(0)
).autoplay()

def stop():
    b1.activate(0)
    metro.stop() # re-activated by previous line :-(
