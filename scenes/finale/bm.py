import pyo
from foococo import foococo as fc
import context
from utils.quickloop import QuickLoop
from autoplayer import Autoplayer

auto = Autoplayer()

short_name = 'FINL'
scrolling = auto.play(fc.Scroller("FINALE"))

volume = pyo.SigTo(pyo.OscReceive(9998, '/volume'), 0.1)
volume.setStopDelay(.1)

# Just placeholders, must be replaced by the right objects on pixies_on()
tabs = [ql.get_tab() for ql in context.pixies]
durs = [ql.get_dur() for ql in context.pixies]

env = pyo.HannTable()
pos = pyo.Phasor([1/d for d in durs], mul=[48000*d for d in durs])
dur = pyo.Noise(.001, .1)
l = pyo.Granulator(tabs, env, .333333, pos, dur, 24, mul=pyo.Fader(.1, .1, mul=.2))
t = pyo.Tone(l.mix(1), 500, mul=volume)

t_send = pyo.Sig(t, mul=.5).stop()

pedal = auto.play(
    pyo.SigTo(context.pedal, .1, mul=.5)
)
loops = [auto.stop(
                QuickLoop(context.mic, i, vol=pedal, listen=auto, min_dur=.5)
            )
                for i in [1,2,6,7]
        ]

def pixies_on():

    # C'est seulement maintenant qu'on peut mettre les bons tabs/durées
    # dans les objets de synthèse. Pour une raison ou une autre, il
    # faut play()er les ql.get_dur() qui ont tendance à être arrêtés...
    # De plus, on répartit les appels dans le temps pour éviter les xruns...

    context.call_asap(
        lambda: l.setTable([ql.get_tab() for ql in context.pixies]),
        lambda: pos.setFreq([1/ql.get_dur().play() for ql in context.pixies]),
        lambda: pos.setMul([48000*ql.get_dur() for ql in context.pixies]),
        t.out,
        lambda: t_send.out(context.FX_LEFT),
    )

def pixies_off():
    
    t_send.stop(.1)


def osc_command(address, *args):
    
    command = args[0]
    if command == 'start':
        pixies_on()
    else:
        pixies_off()

start_stop = pyo.OscDataReceive(9999, '/pixies', osc_command)

@auto.call_on_start
def start():
    context.set_reverb('m')

