import pyo
from foococo import foococo as fc
import context
from autoplayer import Autoplayer
from utils.quickloop import QuickLoop

auto = Autoplayer()

short_name = 'FINL'
scrolling = auto.play(fc.Scroller("FINALE"))


command = auto.play(pyo.OscDataSend('s', 9999, '/pixies'))

vol = auto.play(pyo.OscSend(context.pedal, 9998, '/volume'))

b3 = fc.MultiState(
    fc.button(3),
    [
        [fc.led_on(3, 'red'), lambda: command.send(['stop'])],
        [fc.led_on(3, 'green'), lambda: command.send(['start'])],
    ],
    init=None
)
auto.play(b3)

pedal = auto.play(pyo.SigTo(context.pedal, .1))
loops = [auto.stop(QuickLoop(context.mic, i, vol=pedal, listen=auto)) for i in [1,2,6,7]]

@auto.call_on_start
def start():
    context.b5.activate(1)
    context.set_reverb('m')

