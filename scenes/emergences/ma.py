from foococo import foococo as fc
import context
from autoplayer import Autoplayer

auto = Autoplayer()

short_name = 'EMRG'

scrolling = auto.play(fc.Scroller("EMERGENCES"))

@auto.call_on_start
def start():
    context.b5.activate(0)
    context.set_reverb('bypass')