import pyo
from foococo import foococo as fc
import context
from utils.quickloop import QuickLoop
from autoplayer import Autoplayer

auto = Autoplayer()

short_name = 'EMRG'
scrolling = auto.play(fc.Scroller("EMERGENCES"))

pedal = auto.play(pyo.SigTo(context.pedal, .1))
pedal.setStopDelay(1)
loops = [auto.stop(QuickLoop(context.mic, i, vol=pedal, listen=auto)) for i in [1,2,6,7]]

@auto.call_on_start
def start():
    context.b5.activate(1)
    context.set_reverb('bypass')
