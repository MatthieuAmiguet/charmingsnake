import context
from foococo import foococo as fc
import pyo
from utils.quickloop import QuickLoop
from autoplayer import Autoplayer

auto = Autoplayer()

short_name = 'OUVR'
scrolling = auto.play(
    fc.Scroller("OUVERTURE")
)

pedal = auto.play(
    pyo.SigTo(context.pedal, .1, mul=.5)
)
# Let some time for loops to finish before dropping volume to 0
pedal.setStopDelay(1)

loops = [auto.stop(
                QuickLoop(context.mic, i, vol=pedal, listen=auto, min_dur=.5)
            )
                for i in [1,2,6,7]
        ]

b3 = fc.MultiState(
    fc.button(3),
    [
        [fc.led_on(3, 'red'), lambda: context.mic_send.setValue(0)],
        [fc.led_on(3, 'green'), lambda: context.mic_send.setValue(1)],
    ],
    init=None
)
auto.play(b3)


context.pixies = loops


@auto.call_on_start
def start():
    context.set_reverb('m')
    context.b5.activate(0)
    b3.activate(0)

@auto.call_on_stop
def stop():
    context.mic_send.setValue(1)
