import context
from foococo import foococo as fc
import pyo
from utils.quickloop import QuickLoop
from utils.pst import PST
from autoplayer import Autoplayer

auto = Autoplayer()

short_name = 'OUVR'
scrolling = auto.play(
    fc.Scroller("OUVERTURE")
    )

pedal = auto.play(
    pyo.SigTo(context.pedal, .1)
    )
pedal.allowAutoStart(False)
pedal.setStopDelay(3)


loops = [auto.stop(
            QuickLoop(context.mic, i, vol=pedal, listen=auto)
        )
        for i in [1,2,6,7]
    ]

pitch = pyo.midiToTranspo(60-12-3) # descending minor 3rd
dooooo = auto.stop(
        PST(context.mic, 4, pitch=pitch, vol=pedal, channel=context.DRY_LEFT, fadein=10, fadeout=2, listen=auto)
    )


@auto.call_on_start
def start():
    context.set_reverb('m')
    context.b5.activate(1)


