from autoplayer import Autoplayer

auto = Autoplayer()

short_name = 'IDLE'

@auto.call_on_start
def start():
    print('--- Going idle ---')
