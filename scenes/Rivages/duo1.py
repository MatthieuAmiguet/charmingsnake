from autoplayer import Autoplayer
import context

auto = Autoplayer()

short_name = 'duo1'

mugic2loop = {
    3: 4, # rec to right
    6: 3, # rec left
}

def mugic_moved(m, dir):

    global pos

    if not m in mugic2loop:
        return

    if m == 6:
        dir = -dir

    curpos = pos[m-1]
    newpos = max(min(curpos + dir, 1), -1)
    pos[m-1] = newpos

    print(m, curpos, newpos)

    loop = mugic2loop[m]

    if (curpos, newpos) == (0,1):
        print(f'rec {loop}')
        context.qls[loop-1].rec()
    elif (curpos, newpos) == (1,0):
        print(f'play {loop}')
        context.qls[loop-1].playback()
    elif (curpos, newpos) == (0, -1):
        print(f'stop {loop}')
        context.qls[loop-1].stop()

@auto.call_on_start
def start():

    global pos

    print('Starting scene duo1')
    pos = [0] * 6

    context.mugics.set_callback(mugic_moved)


@auto.call_on_stop
def stop():
    print('Stopping scene duo1')
    context.mugics.set_callback(None)
    for ql in context.qls:
        ql.stop()
