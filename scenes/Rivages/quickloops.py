import context
from foococo import foococo as fc
from autoplayer import Autoplayer
from utils.quickloop import QuickLoop
from utils.vardelay import VarDelay

auto = Autoplayer()

short_name = 'QL'
scrolling = auto.play(fc.Scroller("QUICK LOOPS"))

pedal = context.pedal

loops = [QuickLoop(context.mic, i, vol=pedal, listen=auto) for i in [1,2,6,7]]

vd = VarDelay(context.mic, 3, reset=8, max_length=15, listen=auto)

def reset():
    context.call_asap(
        *[l.stop for l in loops],
        *[l.resync_led for l in loops]
    )

undo_all = fc.Press(
    fc.button('nav_down'),
    reset
)
auto.play(undo_all)


undo_all_global = fc.Press(
    fc.button('nav_up'),
    reset
)


@auto.call_on_start
def start():

    for l in loops:
        l.resync_led()

    vd.reset()

