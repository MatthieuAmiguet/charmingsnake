from autoplayer import Autoplayer
import context

auto = Autoplayer()

short_name = 'rivages'

def mugic_moved(m, dir):

    global pos

    curpos = pos[m-1]
    newpos = max(min(curpos + dir, 1), -1)
    pos[m-1] = newpos

    print(m, curpos, newpos)

    if (curpos, newpos) == (0,1):
        print(f'rec {m}')
        context.qls[m-1].rec()
    elif (curpos, newpos) == (1,0):
        print(f'play {m}')
        context.qls[m-1].playback()
    elif (curpos, newpos) == (0, -1):
        print(f'stop {m}')
        context.qls[m-1].stop()


@auto.call_on_start
def start():

    global pos

    print('Starting scene rivages')
    pos = [0] * 6

    context.mugics.set_callback(mugic_moved)


@auto.call_on_stop
def stop():
    print('Stopping scene rivages')
    context.mugics.set_callback(None)
    for ql in context.qls:
        ql.stop()
