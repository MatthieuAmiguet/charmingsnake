import pyo
from autoplayer import Autoplayer
import context

auto = Autoplayer()

short_name = 'harmonizer'

EX, EY, EZ = context.mugics.m5Euler

third = pyo.Snap(pyo.Scale(EX, 0, 360,2.5, 5.5), [3,4,5])
seventh = pyo.Snap(pyo.Scale(EY, -90, 90, 8.5, 11.5), [9,10,11])
fifth = pyo.Snap(pyo.Scale(EZ, -180, 180, 8.5, 5.5), [6,7,8])
chord =  [third, fifth, seventh]
chord = pyo.SigTo(chord, .1)

envs = [1, .8, .9]

harmonizer = pyo.Harmonizer(context.mics[4], chord, mul=envs).mix(1)
harmonizer = pyo.Compress(harmonizer, thresh=-25, mul=1.3)

auto.out(harmonizer, 4)

@auto.call_on_start
def start():
    print('Starting harmonizer')

@auto.call_on_stop
def stop():
    print('Stopping harmonizer')
