import pyo
from foococo import foococo as fc
import context
from autoplayer import Autoplayer
from utils.scenario import Scenario
from utils.looping import LiveLooper

auto = Autoplayer()

short_name = 'HLVT'

scrolling = auto.play(fc.Scroller("HELVETIC TRIP"))

pedal = pyo.SigTo(context.pedal(1), mul=context.remote['/helv_vol'])
pedal.allowAutoStart(False)
clic = LiveLooper(context.sl_send, mul=pedal, name='CLIC')
bass = LiveLooper(context.ql_send, mul=pedal, name='B')
chords = [LiveLooper(context.ql_send, mul=pedal, play_sync=bass, fadetime=.25, name=f'A{i+1}') for i in range(3)]
hymn = LiveLooper(context.ql_send, mul=pedal, name='HYMN')

b1 = auto.play(fc.Press(fc.button(1)))
b2 = auto.play(fc.Press(fc.button(2)))
b6 = auto.play(fc.Press(fc.button(6)))
b8 = auto.play(fc.Press(fc.button(8)))
suspend_scene = pyo.Trig()


def display(text):
    # Workaround for lousy fc object
    global scrolling
    scrolling.text = text
    scrolling.play()


class HelveticClic(Scenario):

    def setup(self, initial):

        context.select_mic(0) # externe
        fc.led_off(6)()
        clic.stop()
        clic.looper.reset()
        display('GOOD LUCK')

    def steps(self):

        global clic_dur

        yield b6

        display('REC CLIC')
        fc.led_on(6, 'red')()
        clic.rec()
        yield b6

        clic.stop_rec()
        clic_dur = clic.trec['time'].get()/context.server.getSamplingRate()

        display('CLIC OK')

        while True:

            fc.led_on(6, 'green')()
            clic.out()

            # FIXME: if I yield b6, suspend_scene
            # the transition keeps fireing even
            # when suspend_scene is at 0
            yield suspend_scene, b6

            fc.blink(6)()
            clic.stop()

            yield b6

class HelveticSong(Scenario):

    def setup(self, initial):

        fc.led_off(1)()
        for loop in [bass, hymn] + chords:
            loop.stop()
            loop.looper.reset()

        self.recorded = []

        if not initial:
            display('SONG RESET')


    def steps(self):

        yield b1

        for loop in [bass] + chords + [hymn]:

            if loop is bass:
                context.select_mic(1) # interne
                context.dpa.set('voice', 0, .1) #cb
            else:
                context.select_mic(0) # externe


            while True:

                display('REC ' + loop.name)
                fc.led_off(1)()

                for l in self.recorded:
                    l.stop()
                    l.looper.reset()
                    l.out()

                loop.looper.reset()
                loop.rec()

                fc.led_on(1, 'red')()

                if (yield 14 * clic_dur, b2) == 1:
                    for l in self.recorded:
                        l.stop()
                    loop.stop_rec()
                    fc.led_off(1)()
                    display('CANCELED ' + loop.name)
                    yield b1
                    continue

                loop.stop_rec()
                for l in self.recorded:
                    l.stop()
                fc.led_off(1)()
                fc.blink(1)()

                display(loop.name + ' OK')
                if (next:=(yield b1, b8, b2)) == 2:
                    fc.led_off(1)()
                    display('CANCELED ' + loop.name)
                    yield b1
                    continue

                self.recorded.append(loop)
                break

        while True:

            to_play = [bass] + chords
            if next == 1: # b8 was pressed
               to_play.append(hymn)

            for l in to_play:
                l.looper.reset()
                l.out()
            fc.led_on(1, 'green')()

            yield b1, b8

            for l in [bass, hymn] + chords:
                l.stop()
            fc.led_off(1)()
            fc.blink(1)

            next = yield b1, b8


clic_scenario = HelveticClic()
song_scenario = HelveticSong()
b9 = auto.play(fc.Press(fc.button(9), [song_scenario.restart, clic_scenario.restart]))
b4 = auto.play(fc.Press(fc.button(4), song_scenario.restart))

# First trigger suspend_scene and only then stop scenarios.
# That's why I must write it in this order:

@auto.call_on_stop
def exit():
    # stop clic if playing
    suspend_scene.play()

auto.play(clic_scenario)
auto.play(song_scenario)

@auto.call_on_start
def start():
    context.set_reverb('bypass')
