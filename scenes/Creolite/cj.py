import pyo
import context
from foococo import foococo as fc
from utils.quickloop import QuickLoop
from autoplayer import Autoplayer
from utils.mugic import Mugic

auto = Autoplayer()

short_name = 'CJ'

mugic = Mugic()

vol = pyo.Abs(mugic.EZ, mul=.5/180)

delay = auto.out(pyo.Delay(pyo.Sig(context.mic, mul=vol), [.25, .26, .27], [.8, .7, .6 ]))

# FIXME: volume!
pedal = pyo.SigTo(
    pyo.OscReceive(7778, '/pedal/1'), 
    .1, 
    mul=context.remote['/QL_vol']
)
loops = [QuickLoop(context.ql_send, i, vol=pedal, listen=auto) for i in [6,7,8,9]]

b5 = fc.MultiState(
    fc.button(5),
    [
        [fc.led_on(5, 'green'), vol.play],
        [fc.led_on(5, 'red'), vol.stop],
    ],
    init=None,
)
auto.play(b5)

def disp_batt():
    val = int(mugic.Battery.get())
    fc.display(f'{val:3}%')()

batt = auto.play(pyo.Pattern(disp_batt))
