import pyo
import context
from foococo import foococo as fc
from autoplayer import Autoplayer
from utils.quickloop import QuickLoop
from utils.vardelay import VarDelay

auto = Autoplayer()

short_name = 'QL'
scrolling = auto.play(fc.Scroller("QUICK LOOPS"))

pedal = pyo.SigTo(context.pedal(2), mul=context.remote['/QL_vol'])
loops = [QuickLoop(context.ql_send, i, vol=pedal, listen=auto) for i in [1,2,6,7]]

vd = VarDelay(context.ql_send, 3, reset=8, max_length=15, listen=auto)

b4 = fc.MultiState(
    fc.button(4),
    [
        [fc.led_on(4, 'red'), lambda: context.boost.setValue(1)],
        [fc.led_on(4, 'green'), lambda: context.boost.setValue(2)]
    ],
    init=None,
)
auto.play(b4)

b9 = fc.MultiState(
    fc.button(9),
    [
        [fc.led_on(9, 'red'), fc.display('CTRB'), lambda: context.dpa.set('voice', 0, .2)],
        [fc.led_on(9, 'green'), fc.display('BASS'), lambda: context.dpa.set('voice', 1, .2)]
    ],
    init=None,
)
auto.play(b9)


def reset():
    context.call_asap(
        *[l.stop for l in loops],
        *[l.resync_led for l in loops]
    )

undo_all = fc.Press(
    fc.button('nav_down'),
    reset
)
auto.play(undo_all)


undo_all_global = fc.Press(
    fc.button('nav_up'),
    reset
)


@auto.call_on_start
def start():

    for l in loops:
        l.resync_led()

    vd.reset()

    context.select_mic(0) # externe
    context.set_reverb('bypass')
