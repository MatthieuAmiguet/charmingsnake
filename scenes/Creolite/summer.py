from foococo import foococo as fc
from autoplayer import Autoplayer
import context

auto = Autoplayer()

short_name = 'FATA'
scrolling = auto.play(fc.Scroller("ETE"))

@auto.call_on_start
def start():
    context.set_reverb('l')
    context.select_mic(0) # externe
    context.direct_mic_vol.setValue(0)
    context.rev_send_vol.setValue(.5)

@auto.call_on_stop
def stop():
    context.direct_mic_vol.setValue(1)
    context.rev_send_vol.setValue(1)
