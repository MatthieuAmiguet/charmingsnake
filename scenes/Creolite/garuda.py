from foococo import foococo as fc
from autoplayer import Autoplayer
import context
from utils.quickloop import QuickLoop

auto = Autoplayer()

short_name = 'FATA'
scrolling = auto.play(fc.Scroller("GARUDA"))

loop = QuickLoop(context.ql_send, 1, listen=auto, outer_xfade=1)

@auto.call_on_start
def start():
    context.set_reverb('m')
    context.select_mic(.6, update_expr=True) # externe
    context.dpa.set('voice', 0, .1) # cb
