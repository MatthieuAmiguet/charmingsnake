import context
from foococo import foococo as fc
import pyo
from autoplayer import Autoplayer
from utils.vardelay import VarDelay
from utils.taptempo import TapTempo

short_name = 'DRAG'

auto = Autoplayer()

scrolling = auto.play(
    fc.Scroller("DRAGON IVRE")
    )


# ====================================
# 3-3-2
# ====================================

tap = TapTempo(6, init=60.0/96, listen=auto)

beat = tap.value

tocs = [4,7]
tt = pyo.Sig(beat, mul=[(t-1)*.25 for t in tocs])
_delay332 = pyo.SDelay(pyo.Mix([context.mic,context.denorm]), tt, mul=[.9,.8], maxdelay=2)
delay332 = _delay332.mix(1)
_delay_3_beats = pyo.SDelay(pyo.Mix([context.mic,delay332]), pyo.Sig(beat, mul=[2, 4]), mul=[1.1, .9], maxdelay=8)
delay_3_beats = _delay_3_beats.mix(1)

# Optimization: cut the signal chain in two to avoid xruns when stopping it...
# It means that the MultiState b1 below will have some more work to do...
delay_3_beats.allowAutoStart(False)

envelope = pyo.Fader(.1, .2)
gated = pyo.Gate(pyo.Mix([delay332,delay_3_beats]), mul=envelope, thresh=-100)
send_332 = pyo.Sig(gated).stop()

reset332 = auto.play(
    pyo.TrigFunc(envelope['trig'], [_delay332.reset, _delay_3_beats.reset])
)

b1 = fc.MultiState(
    fc.button(1),
    [
        [
            fc.led_on(1, 'red'),
            lambda: send_332.stop(.2),
            lambda: delay_3_beats.stop(.2),
            lambda: _delay_3_beats.stop(.2),
        ],
        [
            fc.led_on(1, 'green'),
            _delay332.reset,
            _delay_3_beats.reset,
            _delay_3_beats.play,
            delay_3_beats.play,
            lambda: gated.out(context.DRY_RIGHT),
            lambda: send_332.out(context.FX_RIGHT)
        ],
    ],
    init=None
)
auto.play(b1)


# ====================================
# "Freeze"
# ====================================

buffer = pyo.NewTable(1)
bufrec = pyo.TableRec(
    pyo.Mix([context.mic, delay332, delay_3_beats]),
    buffer
)
gran = pyo.Granulator(
    table = buffer,
    env = pyo.HannTable(),
    pitch = pyo.Fader(fadein=0.001, fadeout=5, mul=.1, add=.9),
    pos=pyo.Noise(mul=.1*buffer.getRate(), add=.1),
    dur=pyo.Noise(mul=0.001, add=.1),
    grains=100,
    basedur=.1,
    mul=pyo.Fader(fadein=.1, fadeout=5, mul=.1)
)
gran.addLinkedObject(bufrec)

gverb = pyo.Freeverb(gran, size=.8, bal=1, mul=pyo.Fader(.1, 8)).stop()

b2p = fc.Press(
    fc.button(2),
    [
        lambda: gran.out(context.DRY_RIGHT),
        lambda: gverb.out(context.DRY_RIGHT),
        lambda: b1.activate(0),
        fc.led_on(2)
    ],
)
auto.play(b2p)

b3p = fc.Press(
    fc.button(3),
    [lambda: gverb.stop(8), fc.led_off(2)],
)
auto.play(b3p)


# post-freeze

vd = VarDelay(context.mic, 4, reset=9, listen=auto)


@auto.call_on_start
def start():

    context.set_reverb('s')
    context.b5.stop()
    fc.led_off(5)()
    context.mic.setVoice(context.pedal)


@auto.call_on_stop
def stop():

    context.mic.setVoice(0)
    context.b5.play()
    vd.reset()
