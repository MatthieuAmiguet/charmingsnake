from foococo import foococo as fc
from utils.quickloop import QuickLoop
from autoplayer import Autoplayer
import context

auto = Autoplayer()

short_name = 'DRAG'
scrolling = auto.play(fc.Scroller("DRAGON IVRE"))

b3 = fc.MultiState(
    fc.button(3),
    [
        [fc.led_on(3, 'red'), lambda: context.dpa_boost.setValue(1)],
        [fc.led_on(3, 'green'), lambda: context.dpa_boost.setValue(2)],
    ],
    init=None
)
auto.play(b3)


loops = [auto.stop(QuickLoop(context.mic, i, listen=auto, min_dur=.5)) for i in [1,2,6,7]]

@auto.call_on_start
def start():

    context.set_reverb('s')
    context.b5.stop()
    fc.led_off(5)()
    context.mic.setVoice(context.pedal)

@auto.call_on_stop
def stop():
    b3.activate(0)
    context.mic.setVoice(0)
    context.b5.play()
