import pyo
import context
from foococo import foococo as fc
from autoplayer import Autoplayer
from utils.mugic import Mugic

short_name = 'MGIC'

auto = Autoplayer()

harm_vol = 1.3

# FIXME: mugic object is not stoppable (if we stop the OscDataReceive, 
# the messages are queued until it starts again.)

def alive():
    context.nk2.send(['rw', 'flash'])


mugic = Mugic(alive_callback=alive)

dead_spot = pyo.SigTo(pyo.Between(mugic.EY, -90, -65), .1, add=harm_vol, mul=-harm_vol)

third = pyo.Snap(pyo.Scale(mugic.EX, 0, 360,2.5, 5.5), [3,4,5])
seventh = pyo.Snap(pyo.Scale(mugic.EY, -90, 90, 8.5, 11.5), [9,10,11])
fifth = pyo.Snap(pyo.Scale(mugic.EZ, -180, 180, 8.5, 5.5), [6,7,8])
chord =  [third, fifth, seventh]
chord = pyo.SigTo(chord, .1)

third_env = pyo.Follower2(mugic.AY, falltime=1, mul=.2, add=1.5)
seventh_env = pyo.Follower2(mugic.AZ, falltime=1, mul=.2, add=1)
fifth_env = pyo.Follower2(mugic.AX, falltime=1, mul=.2, add=.8)
envs = [third_env, fifth_env, seventh_env]

harmonizer = pyo.Harmonizer(context.mic, chord, mul=envs).mix(1)
harmonizer = pyo.Compress(harmonizer, thresh=-25, mul=dead_spot)
harmonizer = pyo.Gate(harmonizer).stop()
send = pyo.Sig(harmonizer).out(1)



b1 = fc.MultiState(
    fc.button(1),
    [
        [fc.led_on(1, 'red'), harmonizer.stop, send.stop],
        [fc.led_on(1, 'green'), harmonizer.out, lambda: send.out(1)]
    ],
    init=None,
)
auto.play(b1)

b6 = fc.Press(
    fc.button(6),
    [fc.flash(6), mugic.set_EX_origin]
)
auto.play(b6)

def disp_batt():
    val = int(mugic.Battery.get())
    fc.display(f'{val:3}%')()

batt = auto.play(pyo.Pattern(disp_batt))
