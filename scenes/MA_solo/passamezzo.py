from foococo import foococo as fc
import context
import pyo
from autoplayer import Autoplayer
from utils.scenario import Scenario
from utils.looping import LiveLooper

auto = Autoplayer()

short_name = 'MZZO'
scrolling = auto.play(fc.Scroller('PASSAMEZZO'))

pedal = context.pedal
pedal.allowAutoStart(False)
bass = LiveLooper(context.ql_send, mul=pedal)
melody_fader = pyo.SigTo(1, .5, init=1)
melody = LiveLooper(context.ql_send, mul=pedal*melody_fader, play_sync=bass)
chords = [LiveLooper(context.ql_send, mul=pedal, play_sync=bass, fadetime=.25) for _ in range(3)]
drums = LiveLooper(context.ql_send, mul=pedal, play_sync=bass)

b1 = auto.play(fc.Press(fc.button(1)))

class Passamezzo(Scenario):

    def setup(self, initial):

        fc.led_off(1)()
        for l in [bass, melody] + chords:
            l.stop()
            l.looper.reset()
        melody_fader.value = 1
        if not initial:
            context.nk2.send(['stop', 'on'])
        for i in [f'{l}{n}' for l in 'rs' for n in [0,1,2,3]]:
            context.nk2.send([i, 'off'])


    def steps(self):

        yield b1

        fc.led_on(1, 'red')()
        context.nk2.send(['stop', 'off'])
        context.nk2.send(['r0', 'on'])
        bass.rec()
        yield b1

        fc.blink(1)()
        bass.stop_rec()
        bass.out()
        context.nk2.send(['r0', 'off'])
        context.nk2.send(['s0', 'on'])
        context.nk2.send(['r1', 'on'])
        # should be in bass.looper.dur.get(), but for some reason it returns 0
        bass_dur = bass.trec['time'].get()/context.server.getSamplingRate()
        melody.rec()
        yield 3*bass_dur

        fc.led_on(1, 'green')()
        melody.stop_rec()
        melody.out()
        context.nk2.send(['r1', 'off'])
        context.nk2.send(['s1', 'on'])
        context.nk2.send(['r2', 'on'])
        for i in range(3):
            chords[i].rec()
            if i < 2:
                yield bass_dur
            else:
                yield bass_dur - .5
                melody_fader.value = 0
                yield .5

            chords[i].stop_rec()
            chords[i].out()
        melody.stop()
        context.nk2.send(['s1', 'off'])
        context.nk2.send(['r2', 'off'])
        context.nk2.send(['s2', 'on'])
        yield b1

        context.nk2.send(['s3', 'on'])
        context.nk2.send(['r3', 'on'])
        yield bass.looper['trig']

        context.nk2.send(['s3', 'off'])
        drums.rec()
        yield bass.looper['trig']

        drums.stop_rec()
        drums.out()
        context.nk2.send(['s3', 'on'])
        context.nk2.send(['r3', 'off'])
        for i in [bass] + chords:
            i.stop()
        context.nk2.send(['s0', 'off'])
        context.nk2.send(['s2', 'off'])
        yield b1

        context.nk2.send(['r3', 'on'])
        yield drums.looper['trig']
        drums.stop()
        context.nk2.send(['r3', 'off'])
        context.nk2.send(['s3', 'off'])
        for i in [bass] + chords:
            i.out()
        context.nk2.send(['s0', 'on'])
        context.nk2.send(['s2', 'on'])
        yield b1

        context.nk2.send(['s2', 'off'])
        yield bass.looper['trig']

        for i in chords:
            i.stop()

        yield b1



passa = Passamezzo()
b6 = auto.play(fc.Press(fc.button(6), passa.restart))

auto.play(passa)

@auto.call_on_start
def start():
    context.selectMic(0, True, False)
    passa.restart()
    context.nk2.send(['cycle', 'on'])

@auto.call_on_stop
def stop():
    context.nk2.send(['cycle', 'off'])
    context.nk2.send(['stop', 'off'])
