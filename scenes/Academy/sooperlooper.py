from foococo import foococo as fc
import context
from autoplayer import Autoplayer
from utils.sooperwrapper import SLCommand

auto = Autoplayer()

short_name = 'SL'
scrolling = auto.play(fc.Scroller("SOOPERLOOPER"))

sl_controls = SLCommand(
    l1 = context.soo_loops[0],
    l2 = context.soo_loops[1],
    start=auto
)

undo_all_global = fc.Press(fc.button('nav_up'), [
    lambda: sl_controls.loop[0].send(['undo_all']),
    lambda: sl_controls.loop[1].send(['undo_all']),
])

@auto.call_on_start
def start():

    context.sl_feedback = True
    context.sooperlooper.resync_leds()


@auto.call_on_stop
def stop():

    context.sl_feedback = False
