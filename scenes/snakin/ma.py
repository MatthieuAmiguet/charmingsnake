import pyo
from utils.looping import LiveLooper
import context as ctx
from autoplayer import Autoplayer
from foococo import foococo as fc

auto = Autoplayer()

# Display
short_name = 'SNAK'
scrolling = auto.play(fc.Scroller("SNAKIN' AROUND"))


# Loopers
voice = LiveLooper(ctx.mic, max_dur=5, mul=2.2)
bass = LiveLooper(ctx.mic, max_dur=15, play_sync=voice)
wah_control = LiveLooper(ctx.mic, max_dur=15, play_sync=voice)

# Fx
rev = pyo.Freeverb(ctx.mic+ctx.denorm, bal=1, size=.4).stop()
trans = pyo.Harmonizer(bass,transpo=0).stop()
fol = pyo.Follower2(ctx.mic, risetime=.005, falltime=.2, mul=2000, add=200).stop()
wah = pyo.Biquad(trans, freq=fol, q=5, type=2, mul=5).stop()


def reset():
    
    global bar
    bar = 0
    
    for l in [voice, bass, wah_control]:
        l.stop()
        l.looper.reset()
    
    voice.looper.setPitch(1)
    bass.setMul(1)
    
    wah.stop()
    rev.stop()
    
    trans.stop()
    trans.setTranspo(0)
    
    fol.stop()
    fol.setInput(ctx.mic)
    
    ctx.selectMic(0, True)

b1 = fc.MultiState(fc.button(1),
    [
        [fc.led_on(1, 'green'), reset],
        [fc.led_on(1, 'red'), voice.rec],
        [voice.stop_rec, voice.out, bass.rec, lambda: ctx.selectMic(1, True, False)]
    ], init = None
)
auto.play(b1)


# Song structure
def update():
    global bar
    
    bar += 1.0 / voice.looper.pitch
    
    if bar == 4:
        bass.stop_rec()
        # FIXME: better way to do latency compensation?
        # update 2022-05-24: I dont' think this is necessary
        # bass.tab.rotate(-480)
        ctx.selectMic(0, True, False)
        trans.play()
        fol.play()
        wah.out()
        bass.play()
        wah_control.rec()
    elif bar == 8:
        wah_control.stop_rec()
        # FIXME: better way to do latency compensation?
        # follower has a risetime of .005s, which is 240 samples @48'000hz
        wah_control.tab.rotate(-240)
        wah_control.play()
        fol.setInput(wah_control)
        rev.out()
    
    elif bar == 16:
        trans.setTranspo(1)
        voice.looper.setPitch(2)
    
    elif bar == 20:
        trans.setTranspo(0)
        voice.looper.setPitch(1)
    
    elif bar == 23:
        bass.set('mul', 0, .5)
    
    elif bar == 24:
        bass.set('mul', 1, .1)
        bar = 8


tf = auto.play(pyo.TrigFunc(voice.on_loop_start(), update))

@auto.call_on_start
def start():
    ctx.selectMic(0, True, False)
