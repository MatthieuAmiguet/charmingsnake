from foococo import foococo as fc
import pyo

loop = [
    pyo.OscDataSend('s', 9951, '/sl/0/hit'),
    pyo.OscDataSend('s', 9951, '/sl/1/hit')
]

lower_row = [
    fc.Press(fc.button(1), lambda: loop[1].send(['record'])),
    fc.Press(fc.button(2), lambda: loop[1].send(['overdub'])),
    fc.Press(fc.button(3), lambda: loop[1].send(['undo'])),
    fc.Press(fc.button(4), lambda: loop[1].send(['solo'])),
]

upper_row = [
    fc.Press(fc.button(6), lambda: loop[0].send(['record'])),
    fc.Press(fc.button(7), lambda: loop[0].send(['overdub'])),
    fc.Press(fc.button(8), lambda: loop[0].send(['undo'])),
    fc.Press(fc.button(9), lambda: loop[0].send(['solo'])),
]

undo_all = fc.Press(fc.button('nav_down'), [
    lambda: loop[0].send(['undo_all']),
    lambda: loop[1].send(['undo_all']),
])

global_vol = pyo.OscDataSend('sf', 9951, '/sl/-1/set')
