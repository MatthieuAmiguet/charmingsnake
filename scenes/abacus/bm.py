import os
import context
from foococo import foococo as fc
import pyo
from utils.quickloop import QuickLoop
from utils.taptempo import TapTempo
from autoplayer import Autoplayer

auto = Autoplayer()

short_name = 'ABAC'

# No scrolling there:too distracting ;-)


# Crazy dance

root = os.path.dirname(os.path.abspath(__file__))
files = ['%s/sounds/%s.wav' % (root,s) for s in [
    'do_medium_b', 'fadiese_grave_b', 'la_grave_cb', 'mib_aigu_cb'
]]
tables = [pyo.SndTable(f) for f in files]


env = pyo.CosTable([(0,0), (100,.2), (500,2), (1000,.25), (8191,0)])

last_values = (-1,-1)

def display():

    global last_values

    density = min(99, ped.get() * 100)
    vol = min(99, b49.value * 100)

    if (density, vol) == last_values:
        return

    last_values = density, vol

    text = '%2d%2d' % (vol, density)
    fc.display(text)()
    
update_display = auto.play(pyo.Pattern(display, .1))

# For some reason, not wrapping in a Sig yields an error
ped = pyo.Sig(context.pedal)

init_tempo = .8

tap_tempo = TapTempo(1, init=init_tempo, listen=auto)

b6 = fc.MultiState(
    fc.button(6),
    [
        [fc.led_on(6, 'red'), tap_tempo.freeze, lambda: tap_tempo.set(init_tempo)],
        [fc.led_on(6, 'green'), tap_tempo.unfreeze],
    ],
    init=None
)
b6 = auto.play(b6)


speed = pyo.Sig(tap_tempo.value, mul=.25)
metro = auto.play(pyo.Metro([tap_tempo.value, speed]))
slow = metro[0]
pulse = metro[1]

send_speed = auto.play(pyo.OscSend(speed.value, 1234, '/speed'))

visual_metro = auto.play(pyo.TrigFunc(slow, fc.flash(9))) # metronome visuel

loopers = [
    pyo.Looper(
        t,
        start=0,
        dur=t.getDur(),
        xfade=[50,42],
        mul= pyo.Pan(
            pyo.Port(
                pyo.TrigEnv(
                    pyo.Percent(pulse, ped*100),
                    env,
                    mul = pyo.TrigRand(slow, .75, 1, port=1)
                ),
                risetime=.001,
                falltime=.001
            ),
            pan = pyo.SigTo(pyo.TrigChoice(slow, [0,1]))
        )
    )
    for t in tables
]

# NB: In theory, it would not be necessary to auto.play() the loopers as they are played
# by the dance object below. However, starting all these objects in the same callback
# causes dropouts. Adding an explicit auto.play allows to benefit from the "time spreading"
# feature of the autoplayer and avoids dropouts.
for l in loopers:
    auto.play(l)

dance = auto.out(
    pyo.Mix(loopers, voices=2, mul=0),
    inc=1
)

b49 = fc.Expression(
    up = fc.button(9),
    down = fc.button(4),
    callback= [lambda v: dance.set('mul', v/100.0, .1)]
)
auto.play(b49)

# Reverb

b3 = fc.MultiState(
    fc.button(3),
    [
        [fc.led_on(3, 'red'), lambda: context.mic_send.setValue(0)],
        [fc.led_on(3, 'green'), lambda: context.mic_send.setValue(1)],
    ],
    init=None
)
auto.play(b3)

# QuickLoop

loops = [auto.stop(QuickLoop(context.mic, i, listen=auto, min_dur=.5)) for i in [2,7]]

loop_send = auto.out(pyo.Mix([l.output for l in loops]), context.FX_LEFT)

@auto.call_on_start
def start():
    context.b5.activate(0)
    context.set_reverb('m')

@auto.call_on_stop
def stop():
    context.mic_send.setValue(1)
