import os
import context
from foococo import foococo as fc
import pyo
from autoplayer import Autoplayer

short_name = 'ABAC'

auto = Autoplayer()

root = os.path.dirname(os.path.abspath(__file__))
sound_files = ['do_medium_b', 'mib_aigu_cb', 'la_grave_cb', 'fadiese_grave_b']
s1, s2, s3, s4 = [pyo.SndTable("%s/sounds/%s.wav" % (root, f)) for f in sound_files]

def scene1():
    texture1.setTable(s3)
    texture2.setTable(s4)

def scene2():
    texture1.setTable(s1)
    texture2.setTable(s2)

pedal = auto.play(pyo.SigTo(context.pedal, .5))
pedal.setStopDelay(2)

texture1 = pyo.Looper(
    s1, start=0, dur=s1.getDur(), xfade=[50, 42],
    mul=pyo.Min(pyo.Sub(1,pedal, mul=2), 1)
)
texture2 = pyo.Looper(
    s2, start=0, dur=s2.getDur(), xfade=[50, 42],
    mul=pyo.Min(pyo.Sig(pedal, mul=2), 1)
)

c = pyo.Chorus(pyo.Mix([texture1,texture2], voices=1), mul=0)
lfo2 = pyo.FastSine(.2, mul=.2, add=.1)
lfo1 = pyo.FastSine(lfo2)
env = pyo.Fader(1,1, mul=2)
r = pyo.Pan(c, outs=2, pan=lfo1, mul=env)
r.setStopDelay(1)
auto.out(r, channel=0, inc=1)

# TODO: understand why sound is abruptly cut on tune change

texture_send = auto.out(
    pyo.Sig(r, mul=.7),
    channel = 2, inc=0
)


b7 = fc.MultiState(
    fc.button(7),
    [
        [fc.led_on(7, 'green'), fc.display('A-F#'), scene1],
        [fc.led_on(7, 'red'), fc.display('C-Eb'), scene2],
    ],
    init = None
)
auto.play(b7)

vol = fc.Expression(
        up = fc.button(6),
        down = fc.button(1),
        callback= [fc.display('D'), lambda v: c.set('mul', v/100.0, .1)]
)
auto.play(vol)

b3 = fc.MultiState(
        fc.button(3),
        [
            [fc.led_on(3, 'red'), lambda: context.dpa_boost.setValue(1), lambda: context.set_reverb('m')],
            [fc.led_on(3, 'green'), lambda: context.dpa_boost.setValue(2), lambda: context.set_reverb('l')],
        ],
        init=None
    )
auto.play(b3)

lfos = pyo.FastSine([.19,.2,.21,.23], mul=.5, add=.5)
get_speed = pyo.OscReceive(1234, '/speed')
speeds = pyo.Max( # to avoid 0 if no OSC messages comes in
    get_speed['/speed'],
    .05,
    mul=[1,2,3,4]
)
speeds.addLinkedObject(get_speed) # stop get_speed when stopping speed
delays = pyo.Delay(pyo.Denorm(context.mic), speeds, 0, mul=lfos)
d_out = delays.mix(2) # makes a nice ping-pong delay :-)
auto.play(d_out)

b8 = fc.MultiState(
        fc.button(8),
        [
            [fc.led_on(8, 'red'), d_out.stop, delays.reset],
            [fc.led_on(8, 'green'), d_out.out],
        ],
        init = None
    )
auto.play(b8)

@auto.call_on_start
def start():
    context.b5.activate(1)
    context.set_reverb('m')

@auto.call_on_stop
def stop():
    b3.activate(0)
