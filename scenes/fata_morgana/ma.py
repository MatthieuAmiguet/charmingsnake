# coding: utf-8

import math
import context
from foococo import foococo as fc
import pyo
from autoplayer import Autoplayer

short_name = 'FATA'

auto = Autoplayer()

pedal = context.pedal

def senn_only():
    
    fc.display('S  -')()
    context.mic.set('voice', 0, .1)
    context.mic_boost.setValue(1)

# FIXME: not 100% sure about this. But empirically verified and seconded by
# https://dsp.stackexchange.com/a/14755
boost = math.sqrt(2)

def both_mics():
    
    fc.display('S  D')()
    context.mic.set('voice', .5, .1)
    context.mic_boost.setValue(boost)

def dpa_only():
    
    fc.display('-  D')()
    context.mic.set('voice', 1, .1)
    context.mic_boost.setValue(1)


low_thresh = pyo.Thresh(pedal, .15, 1)
med_thresh = pyo.Mix([pyo.Thresh(pedal, .15, 0), pyo.Thresh(pedal, .85, 1)])
high_thresh = pyo.Thresh(pedal, .85, 0)

trigfuncs = [
    pyo.TrigFunc(low_thresh, senn_only),
    pyo.TrigFunc(med_thresh, both_mics),
    pyo.TrigFunc(high_thresh, dpa_only),
]

for t in trigfuncs:
    auto.play(t)


# À la fin, on noye la mélodie dans la réverbe:

orig_dry_time = context.dry_level.time
leaving_time = 10

red_light = pyo.CallAfter(fc.led_on(1, 'red'), time=leaving_time).stop()

def there():
    red_light.stop()
    fc.led_on(1, 'green')()
    context.dry_level.setTime(orig_dry_time)
    context.dry_level.setValue(1)

def leaving():
    fc.blink(1, 'red')()
    red_light.play()
    context.dry_level.setTime(leaving_time)
    context.dry_level.setValue(0)

b1 = fc.MultiState(
        fc.button(1),
        [
            there,
            leaving,
        ],
        init=None
    )
auto.play(b1)

@auto.call_on_start
def start():
    
    context.set_reverb('l')
    both_mics()
    context.b5.stop()
    fc.led_off(5)()
    b1.activate(0)
    context.dpa_boost.setValue(2)
    
@auto.call_on_stop
def stop():
    
    senn_only()
    context.b5.play()
    context.dpa_boost.setValue(1)
    there()