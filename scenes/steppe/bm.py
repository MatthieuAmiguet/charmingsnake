import os
import context
from foococo import foococo as fc
import pyo
from autoplayer import Autoplayer

auto = Autoplayer()

short_name = 'STEPP'
scrolling = auto.play(fc.Scroller("STEPPE DE CHEVAL"))

# TAMBOUR ======================================

root = os.path.dirname(os.path.abspath(__file__))
poum = pyo.SndTable(root+"/sounds/poum.wav")

beat = pyo.Beat(time=1.5, taps=8, w1=100, w2=0, w3=60)
tambour = pyo.TrigEnv(beat, poum, dur=1, mul=.3)

# NB: I was first using a single-channel-reverb going through a Pan/SPan, but for some reason
# starting/stopping the chain was causing dropouts, whatever I tried.
# Replacing by two identical reverb channels (which should be more expensive) solved the problem (WTF??)
rev = pyo.Freeverb(pyo.Mix([tambour, context.denorm]), size=[.9, .9], damp=.9, bal=.40,mul=pyo.Fader(.1, 1)).stop()

b2 = fc.MultiState(
    fc.button(2),
    [
        [fc.led_on(2, 'red'), lambda: rev.stop(1)],
        [fc.led_on(2, 'green'), lambda: rev.out(inc=1)],
    ],
    init=None
)
auto.play(b2)


x = 0
def change():
    global x
    x = (x + 1) % 4
    if x == 0:
        beat.new()

bar = auto.play(pyo.TrigFunc(beat['end'][0], change))

# OMMMMMMMM ======================================

om = pyo.SndTable(root+"/sounds/om.wav")
texture = pyo.Looper(
    om, start=0, dur=om.getDur(), xfade=[50, 42], mul=pyo.Fader(1, .5, mul=.2)
).mix(1)
tex_pan = pyo.SPan(texture)
tex_rev = pyo.Freeverb(texture, mul=pyo.Fader(.1, 3), size=1, damp=.8, bal=1).stop()

b1 = fc.MultiState(
    fc.button(1),
    [
        [fc.led_on(1, 'green'), lambda: tex_rev.out(inc=1)],
        [fc.led_on(1, 'red'), lambda: tex_rev.stop(3)],
    ],
    init=None
)
auto.play(b1)


# Reverb

b3 = fc.MultiState(
    fc.button(3),
    [
        [fc.led_on(3, 'green'), lambda: context.mic_send.setValue(1)],
        [fc.led_on(3, 'red'), lambda: context.mic_send.setValue(0)],
    ],
    init=None
)
auto.play(b3)


@auto.call_on_start
def start():

    context.set_reverb('m')
    context.b5.activate(0)
    b1.activate(0)

@auto.call_on_stop
def stop():
    
    # stop ommmmm and poum
    b1.activate(1)
    b2.activate(0)
    
    context.mic_send.setValue(1)
