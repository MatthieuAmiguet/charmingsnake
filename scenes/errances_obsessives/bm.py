from foococo import foococo as fc
import context
from autoplayer import Autoplayer

auto = Autoplayer()

short_name = 'EROB'
scrolling = auto.play(fc.Scroller("ERRANCES OBSESSIVES"))

# fc controls -> SooperLooper
from ..common.sooperlooper import loop, lower_row, upper_row, undo_all

for o in lower_row + upper_row + [undo_all]:
    auto.play(o)


# Explicit start/stop

@auto.call_on_start
def start():

    context.set_reverb('bypass')
    context.b5.activate(0)
    context.b5.stop()
    fc.led_off(5)()

    context.mic_send.setValue(0)

    context.sl_feedback = True
 

@auto.call_on_stop
def stop():
    
    context.b5.play()
    context.sl_feedback = False
    context.mic_send.setValue(1)
    for l in loop:
        l.send(['undo_all'])
