This repository provides the software infrastructure underlying the *augmented instruments* performances at [Les Chemins de Traverse](https://www.lescheminsdetraverse.net/) since 2015.

To get an idea of what can be done with it, the best thing is probably to go and see a live performance of Les Chemins de Traverse, the next best thing being to listen to the album [Dragonfly](https://lescheminsdetraverse.bandcamp.com/album/dragonfly) (also available on all major streaming platforms if you want to support majors instead of artists).

It is, and will probably remain, a constant work in progress.

The complete code is probably only useful for people sharing both the same hardware (controllers, sound interface, ...) and same musical ideas as we do.

However, some of the ideas and code here could be of interest for other people. In particular:

- The whole architecture, based on a set/track/scene distinction, is an attempt at solving "The Gig Problem", that is the smooth transition between several "scenes" with different setups in a live context. This is described in [my Europython 2019 talk](https://www.matthieuamiguet.ch/blog/europython19-talk-online).
  
- The use of generators to write readable scenarios based on callbacks is presented in my PyConDE 2024 talk. The relevant code is in `utils/scenario.py`
  
- Pyo users that get audio dropouts on python callbacks could be interested in the "time spreading" features implemented in `track:call_asap(*funcs)` and related. This is not yet documented anywhere, although I plan to write a blog post about this one day...
