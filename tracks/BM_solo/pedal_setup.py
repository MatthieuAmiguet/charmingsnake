from functools import partial
from pythonosc.udp_client import SimpleUDPClient

bmsolo_port = ("127.0.0.1", 7777)
sooloo_port = ("127.0.0.1", 9951)

bmsolo = SimpleUDPClient(*bmsolo_port)
sooloo = SimpleUDPClient(*sooloo_port)

noop = lambda x: ...

ped2sends = [
    partial(bmsolo.send_message, '/pedal/1'),
    lambda v: sooloo.send_message('/sl/-1/set', ['wet', v]),
    noop,
    noop,
]

inits = [1, 1, 0, 0]
