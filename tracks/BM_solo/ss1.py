import pyo
from foococo import foococo as fc
import context

scenes = [
]

controller_index = 1
controller_model = 1

jackname = 'ss1'

init_message = 'RVRB'

def between(old, new):
    pass

in_ports = [
]
out_ports = [
]


def set_reverb(i, type):
    fc.led_off([1,2,3,4])()
    fc.led_on(i)()
    set_reverb.sender.send([type])

def setup():

    set_reverb.sender = pyo.OscDataSend('s', 3456, '/reverb/1')
    set_reverb(1, 'bypass') # no reverb

    context.b1 = fc.Press(fc.button(1), lambda: set_reverb(1, 'bypass'))
    context.b2 = fc.Press(fc.button(2), lambda: set_reverb(2, 's'))
    context.b3 = fc.Press(fc.button(3), lambda: set_reverb(3, 'm'))
    context.b4 = fc.Press(fc.button(4), lambda: set_reverb(4, 'l'))
