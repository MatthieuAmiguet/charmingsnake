import pyo
from foococo import foococo as fc
import context

LOOPS_VOLUME = 1.4 # <--- Adjust loops volume here! :-)

context.set_name = 'BM_solo'

scenes = [
    'BM_solo.choral',
    'BM_solo.sooperlooper',
    'BM_solo.quickloops',
    'canon',
    'pachelbel.bm'
]

jackname = 'bm_solo'

controller_index = 2
controller_model = 2

in_ports = [
    ['system:capture_1'], # contrabass dpa
    ['system:capture_5'], # channel 2 is kaputt 
    ['system:capture_3']  # bass dpa
]
out_ports = [
    ['system:playback_1', 'system:playback_3'], # main out, send to amp + reverb
    ['sooperlooper:common_in_1'], # send to sooperlooper
]

init_message = 'BM SOLO'

dpa_cb_mul = .7
dpa_b_mul = .2

def sl_setup():
    
    global register
    register = pyo.OscDataSend('siss', 9951, [ '/sl/0/register_auto_update','/sl/1/register_auto_update'])
    register.send(['state',100,'localhost:9901', '/loop_state'])

    global get_state
    get_state = pyo.OscDataReceive(9901, '/loop_state', loop_state)


def setup():

    context.pedal = pyo.SigTo(pyo.OscReceive(7777, '/pedal/1'), .1)

    sl_setup()

    context.boost = pyo.SigTo(1, .2, 1)
    
    dpa_cb = pyo.Input(0, mul=dpa_cb_mul)
    sennheiser = pyo.Input(1)
    dpa_b = pyo.Input(2, mul=dpa_b_mul)
    
    dpa = pyo.Selector([dpa_cb, dpa_b], voice=0, mul=context.boost)
    
    mic = pyo.Selector([sennheiser, dpa], voice=0, mul=0).out(0)
    sl_send = pyo.Sig(mic, mul=LOOPS_VOLUME).out(1) # send louder signal to sooperlooper
    ql_send = pyo.Sig(mic, mul=LOOPS_VOLUME) # send louder signal to quickloops

    volume = pyo.Follower(dpa+sennheiser)
    thresh = pyo.Thresh(volume, .005)
    monitor = pyo.TrigFunc(thresh, fc.flash(10))

    b0 = fc.MultiState(
        fc.button(0),
        [
            [monitor.play, fc.led_on(0, 'red'), lambda: mic.set('mul',0, .1)],
            [monitor.stop, fc.led_on(0, 'green'), lambda: mic.set('mul', 1, .1)],
        ],
    )


    # FIXME: this will probably be replaced by a physical expression pedal
    def selectMic(value, update_expr=False):
        
        if update_expr:
            expr.value = value
        
        mic.set('voice', value, port=0.2)
        
        to_display = int((1 - value) * 100)
                
        fc.display('M')(to_display)
        
        if value == 0:
            fc.led_on(5, 'green')()
        elif value == 1:
            fc.led_on(5, 'red')()
        else:
            fc.led_off(5)()

    expr = fc.Expression(
            up = fc.button(5, 'b'),
            down = fc.button(5, 't'),
            callback = lambda val: selectMic(val/100.0),
            max_speed = .02,
            )

    b5 = [
        fc.Press(fc.button(5, 'l'), lambda: selectMic(1, update_expr=True), threshold=.95),
        fc.Press(fc.button(5, 'r'), lambda: selectMic(0, update_expr=True), threshold=.95),
        expr,
    ]
    
    selectMic(1)
    
    denorm = pyo.Noise(1e-24)

    context.dpa = dpa
    context.mic = mic
    context.sl_send = sl_send
    context.ql_send = ql_send
    context.b0 = b0
    context.b5 = b5
    context.denorm = denorm
    

between = lambda: fc.led_off([1,2,3,4,6,7,8,9])()

# SooperLooper State -> fc leds

context.sl_feedback = False


sl_states = [0,0]

def loop_state(address, *args):
    
    if not context.sl_feedback:
        return
    
    loop_index, control, value = args
    
    if control != u'state':
        print("Unknown control: %s" % control)
        return

    sl_states[loop_index] = value
    
    # loops are numbered from top, buttons from bottom...
    base = 6 if loop_index==0 else 1
    
    if value == 0:
        fc.led_off([i+base for i in range(4)])()
    
    elif value == 1: # wait start
        fc.blink(base, 'red')()
    
    elif value == 2: # recording
        fc.led_on(base, 'red')()
        
    elif value == 3: # wait stop
        fc.blink(base, 'green')()
    
    elif value == 4: # playing
        fc.led_on(base, 'green')()
        fc.led_off([base+1, base+2])()
        fc.led_on(base+3, 'green')()
        
    elif value == 5: # overdubbing
        fc.led_on(base, 'green')()
        fc.led_on(base + 1, 'red')()
    
    elif value == 10: # muted
        # if muted, the other one is soloed...
        fc.led_off(base+3)()

    else:
        print("Unhandled message: %s, %s, %s" % (loop_index, control, value))


def resync_sl_leds():
    
    for i, s in enumerate(sl_states):
        
        loop_state(None, i, 'state', s)

context.resync_sl_leds = resync_sl_leds
