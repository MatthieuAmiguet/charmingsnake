import pyo
import nk2

s = pyo.Server(audio='jack', nchnls=0)
nk2.init(s)
s.boot()
f1 = nk2.fader(0)
send = pyo.OscSend(f1, 8888, '/nk2/f1')
s.start()
