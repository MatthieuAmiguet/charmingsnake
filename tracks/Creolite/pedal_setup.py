from functools import partial
from pythonosc.udp_client import SimpleUDPClient

bmsolo_port = ("127.0.0.1", 7777)
cj_port = ("127.0.0.1", 7778)
sooloo_port = ("127.0.0.1", 9951)

bmsolo = SimpleUDPClient(*bmsolo_port)
sooloo = SimpleUDPClient(*sooloo_port)
cj = SimpleUDPClient(*cj_port)

noop = lambda x: ...

def send_bm_cj(m):
    bmsolo.send_message('/pedal/1', m)
    cj.send_message('/pedal/1', m)

ped2sends = [
    partial(bmsolo.send_message, '/pedal/0'),
    send_bm_cj,
    lambda v: sooloo.send_message('/sl/1/set', ['wet', v]),
    lambda v: sooloo.send_message('/sl/0/set', ['wet', v]),
]

inits = [1, 1, 1, 1]
