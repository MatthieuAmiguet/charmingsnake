import pyo
from foococo import foococo as fc
import context

LOOPS_VOLUME = 1.5 # <--- Adjust loops volume here! :-)

scenes = [
    'Creolite.cj'
]

controller_index = 1
controller_model = 1

jackname = 'cj'

init_message = 'CREOLITE - CJ'

def between(old, new):
    pass

in_ports = [
    ['system:capture_6'], # sennheiser headset
]
out_ports = [
    ['system:playback_1','system:playback_2'], # main out, send to amp (no reverb)
    ['sooperlooper:loop0_in_1'], # send to sooperlooper
]


def set_reverb(i, type):
    fc.led_off([1,2,3,4])()
    fc.led_on(i)()
    set_reverb.sender.send([type])

def setup():

    set_reverb.sender = pyo.OscDataSend('s', 3456, '/reverb/1')
    set_reverb(1, 'bypass') # no reverb

    context.b1 = fc.Press(fc.button(1), lambda: set_reverb(1, 'bypass'))
    context.b2 = fc.Press(fc.button(2), lambda: set_reverb(2, 's'))
    context.b3 = fc.Press(fc.button(3), lambda: set_reverb(3, 'm'))
    context.b4 = fc.Press(fc.button(4), lambda: set_reverb(4, 'l'))

    addrs = ['/QL_vol', '/direct_vol']
    context.remote = pyo.OscReceive(9998, addrs)
    context.remote.allowAutoStart(False)
    context.get_state = pyo.OscDataSend('', 8080, '/STATE/SEND')
    # open-stage-control needs some time to start
    context.get_state_later = pyo.Pattern(context.get_state.send, 3, [[]]).play()

    vol = pyo.SigTo(
        pyo.OscReceive(8888, '/nk2/f1'), 
        .1, 
        mul=context.remote['/direct_vol']
    )
    context.mic = pyo.Input(0, mul=vol).out(0)
    context.sl_send = pyo.Sig(context.mic, mul=LOOPS_VOLUME).out(1) # send louder signal to sooperlooper
    context.ql_send = pyo.Sig(context.mic, mul=LOOPS_VOLUME) # send louder signal to sooperlooper
    
