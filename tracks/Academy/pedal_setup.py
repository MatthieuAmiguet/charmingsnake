from functools import partial
from pythonosc.udp_client import SimpleUDPClient

poste1_port = ("127.0.0.1", 7777)
poste2_port = ("127.0.0.1", 7778)
sooloo_port = ("127.0.0.1", 9951)

poste1 = SimpleUDPClient(*poste1_port)
poste2 = SimpleUDPClient(*poste2_port)
sooloo = SimpleUDPClient(*sooloo_port)

def set_loops_vol(*loops):

    def inner(vol):
        for i in loops:
            sooloo.send_message(f'/sl/{i}/set', ['wet', vol]),

    return inner

ped2sends = [
    partial(poste1.send_message, '/pedal/1'),
    set_loops_vol(0,1),
    partial(poste2.send_message, '/pedal/1'),
    set_loops_vol(2,3),
]

inits = [1, 1, 1, 1]
