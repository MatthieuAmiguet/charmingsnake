import sys
import pyo
from foococo import foococo as fc
from utils.sooperwrapper import SLFeedback
import context

try:
    # argv[1] will be the name of the track, paremeters start at 2
    ss_index = int(sys.argv[2])
except:
    print('Error: Missing parameter.')
    print('Usage: track ssn <n>')
    sys.exit(1)

LOOPS_VOLUME = 1.5 # <--- Adjust loops volume here! :-)

context.set_name = 'Academy'

scenes = [
    'Academy.sooperlooper',
    'BM_solo.quickloops',
    'canon',
    'pachelbel.bm',
    'MA_solo.passamezzo',
    'snakin.ma',
    'BM_solo.choral',
]

jackname = f'ss{ss_index}'

controller_index = ss_index
controller_model = ss_index

sennheiser_in_channel = 4 if ss_index == 1 else 5
main_out_channel = 2 if ss_index == 1 else 1
rev_out_channel = main_out_channel + 2

loops = (1,0) if ss_index == 2 else (3,2)

in_ports = [
    ['system:capture_1'], # contrabass dpa
    [f'system:capture_{sennheiser_in_channel}'], 
    ['system:capture_3']  # bass dpa
]
out_ports = [
    [f'system:playback_{main_out_channel}', f'system:playback_{rev_out_channel}'], # main out, send to amp + reverb
    [f'sooperlooper:loop{n}_in_1' for n in loops], # send to sooperlooper
]

init_message = 'POSTE 1' if ss_index == 2 else 'POSTE 2'

dpa_cb_mul = .35
dpa_b_mul = .2

def setup():

    port = 7777 if ss_index == 2 else 7778
    context.pedal = pyo.SigTo(pyo.OscReceive(port, '/pedal/1'), .1)

    soo_port = 9901 if ss_index == 2 else 9902
    context.sooperlooper = SLFeedback(l1=loops[0], l2=loops[1], inport=soo_port)
    context.soo_loops = loops

    context.boost = pyo.SigTo(1, .2, 1)
    
    dpa_cb = pyo.Input(0, mul=dpa_cb_mul)
    sennheiser = pyo.Input(1)
    dpa_b = pyo.Input(2, mul=dpa_b_mul)
    
    init_voice = 0 if ss_index == 2 else 1
    dpa = pyo.Selector([dpa_cb, dpa_b], voice=init_voice, mul=context.boost)
    
    mic = pyo.Selector([sennheiser, dpa], voice=0, mul=0).out(0)
    sl_send = pyo.Sig(mic, mul=LOOPS_VOLUME).out(1) # send louder signal to sooperlooper
    ql_send = pyo.Sig(mic, mul=LOOPS_VOLUME) # send louder signal to quickloops

    volume = pyo.Follower(dpa+sennheiser)
    thresh = pyo.Thresh(volume, .005)
    monitor = pyo.TrigFunc(thresh, fc.flash(10))

    b0 = fc.MultiState(
        fc.button(0),
        [
            [monitor.play, fc.led_on(0, 'red'), lambda: mic.set('mul',0, .1)],
            [monitor.stop, fc.led_on(0, 'green'), lambda: mic.set('mul', 1, .1)],
        ],
    )


    # FIXME: this will probably be replaced by a physical expression pedal
    def selectMic(value, update_expr=False, display=True):

        if update_expr:
            expr.value = value

        mic.set('voice', value, port=0.2)

        if display:
            to_display = int((1 - value) * 100)
            fc.display('M')(to_display)

        if value == 0:
            fc.led_on(5, 'green')()
        elif value == 1:
            fc.led_on(5, 'red')()
        else:
            fc.led_off(5)()

    expr = fc.Expression(
            up = fc.button(5, 'b'),
            down = fc.button(5, 't'),
            callback = lambda val: selectMic(val/100.0),
            max_speed = .02,
            )

    b5 = [
        fc.Press(fc.button(5, 'l'), lambda: selectMic(1, update_expr=True), threshold=.95),
        fc.Press(fc.button(5, 'r'), lambda: selectMic(0, update_expr=True), threshold=.95),
        expr,
    ]
    
    selectMic(1)
    
    denorm = pyo.Noise(1e-24)

    context.dpa = dpa
    context.mic = mic
    context.sl_send = sl_send
    context.ql_send = ql_send
    context.b0 = b0
    context.b5 = b5
    context.denorm = denorm
    context.selectMic = selectMic

    context.nk2 = pyo.OscDataSend('ss', 4567, '/nk2/leds')
    

between = lambda: fc.led_off([1,2,3,4,6,7,8,9])()
