import time
import pyo
import nk2
from utils.mugic import Mugic

server = pyo.Server(audio='jack', ichnls=1, nchnls=4, jackname='nk2')
server.setJackAuto(False, False)
server.setJackAutoConnectInputPorts([
    ['system:capture_6'],
])
server.setJackAutoConnectOutputPorts([
    ['system:playback_1'], # out left
    ['system:playback_2'], # out right
    ['system:playback_3'], # rev left
    ['system:playback_4'], # rev right
])
nk2.init(server)
server.boot()

# Set leds via OSC

def setleds(address, *args):

    led, action = args

    if action == 'on':
        nk2.led_on(led)
    elif action == 'off':
        nk2.led_off(led)

receive = pyo.OscDataReceive(4567, '/nk2/leds', setleds)


# Manage Sooperlooper sync (not nk2-related!)

inport = 9955
soopl = pyo.OscDataSend('sf', 9951, '/set')
states = [0] * 4

def loop_state(_address, *args):

    loop_index, control, value = args

    # If everything is stopped and I start recording on loop #1 or #3,
    # set sync to that loop
    if value == 2 and loop_index in [1, 3] and all([s in [0, 20] for s in states]):
        soopl.send(['sync_source', loop_index+1])

    states[loop_index] = value

    # If everything is stopped, set sync to None
    if all([s in [0, 20] for s in states]):
        soopl.send(['sync_source', 0])


register = pyo.OscDataSend(
    'siss', 
    9951, 
    [f'/sl/{i}/register_auto_update' for i in range(4)]
)
register.send(['state',100,f'localhost:{inport}', '/loop_state'])
get_state = pyo.OscDataReceive(inport, '/loop_state', loop_state)

# Reverb

def set_reverb(i, type):
    for j in range(4):
        nk2.led_off(f'r{j}')
    nk2.led_on(f'r{i}')
    set_reverb.sender.send([type])

set_reverb.sender = pyo.OscDataSend('s', 3456, '/reverb/1')
r0 = pyo.TrigFunc(nk2.Press("r0"), lambda: set_reverb(0, 'bypass'))
r1 = pyo.TrigFunc(nk2.Press("r1"), lambda: set_reverb(1, 's'))
r2 = pyo.TrigFunc(nk2.Press("r2"), lambda: set_reverb(2, 'm'))
r3 = pyo.TrigFunc(nk2.Press("r3"), lambda: set_reverb(3, 'l'))

# sound through

mic = pyo.Input(0)
pan = pyo.Pan(mic, mul=nk2.fader(0)).out()
send_rev = pyo.Sig(pan).out(2)

# harmonizer

def alive():
    nk2.led_blink('rw')

mugic = Mugic(alive_callback=alive)

third = pyo.Snap(pyo.Scale(mugic.EX, 0, 360,2.5, 5.5), [3,4,5])
seventh = pyo.Snap(pyo.Scale(mugic.EY, -90, 90, 8.5, 11.5), [9,10,11])
fifth = pyo.Snap(pyo.Scale(mugic.EZ, -180, 180, 8.5, 5.5), [6,7,8])
chord =  [third, fifth, seventh]
chord = pyo.SigTo(chord, .1)

envs = [nk2.fader(i) for i in [1,2,3]]

harmonizer = pyo.Harmonizer(mic, chord, mul=envs).mix(1)
harmonizer = pyo.Compress(harmonizer, thresh=-25, mul=1.3)
# harmonizer = pyo.Gate(harmonizer)
harm_pan = pyo.Pan(harmonizer).out(0)
harm_send= pyo.Sig(harm_pan).out(2)

# Battery feedback

batt_warning = pyo.Pattern(lambda: nk2.led_blink('r7'), .5)


def disp_battery():

    level = mugic.Battery.get()
    print('Battery Level: ', level)

    for thresh, led in [(25, 'r7'), (50, 'm7'), (75, 's7')]:

        if level >= thresh:
            nk2.led_on(led)
        else:
            nk2.led_off(led)

    if level < 15:
        batt_warning.play()
    else:
        batt_warning.stop()

disp_batt = pyo.Pattern(disp_battery, 5).play()

# Main program

server.start()
nk2.leds_external()
time.sleep(.1)
set_reverb(0, 'bypass')

while True:
    time.sleep(1)
