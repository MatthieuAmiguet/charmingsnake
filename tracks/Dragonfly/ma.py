# coding: utf-8

import context

jackname = 'ma'

controller_index = 1
controller_model = 1

in_ports = [
    ['system:capture_3'],
    ['system:capture_4']
]

# Inverted left/right so that the "default side" is right!

out_ports = [
    ['system:playback_2'], # dry right
    ['system:playback_1'], # dry left
    ['system:playback_6'], # fx send right
    ['system:playback_5'], # fx send left
]

context.pedal_osc_port = 7778

context.DRY_RIGHT, \
context.DRY_LEFT, \
context.FX_RIGHT, \
context.FX_LEFT = range(4)

init_message = 'DRAGONFLY / MA'

context.reverb_osc_address = '/reverb/2'

# Common to BM & MA
from . import common

scenes = [scene+'.ma' for scene in common.scenes]

setup = common.setup
between = common.between

context.dpa_mul = .4
context.gated = False
