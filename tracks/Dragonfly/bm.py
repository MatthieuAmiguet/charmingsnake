# coding: utf-8

import pyo
import context
from foococo import foococo as fc

jackname = 'bm'

controller_index = 2
controller_model = 2

in_ports = [
    ['system:capture_1'],
    ['system:capture_5']
]
out_ports = [
    ['system:playback_1'], # dry left
    ['system:playback_2'], # dry right
    ['system:playback_3'], # fx send left
    ['system:playback_4'], # fx send right
]

context.pedal_osc_port = 7777

context.DRY_LEFT, \
context.DRY_RIGHT, \
context.FX_LEFT, \
context.FX_RIGHT = range(4)


init_message = 'DRAGONFLY / BM'

# Sooperlooper
# ============

def sl_setup():

    global register
    register = pyo.OscDataSend('siss', 9951, [ '/sl/0/register_auto_update','/sl/1/register_auto_update'])
    register.send(['state',100,'localhost:9901', '/loop_state'])

    global get_state
    get_state = pyo.OscDataReceive(9901, '/loop_state', loop_state)


context.sl_feedback = False

def loop_state(address, *args):
    
    if not context.sl_feedback:
        return
    
    loop_index, control, value = args
    
    if control != u'state':
        print("Unknown control: %s" % control)
        return
    
    # loops are numbered from top, buttons from bottom...
    base = 6 if loop_index==0 else 1
    
    if value == 0:
        fc.led_off([i+base for i in range(4)])()
    
    elif value == 1: # wait start
        fc.blink(base, 'red')()
    
    elif value == 2: # recording
        fc.led_on(base, 'red')()
        
    elif value == 3: # wait stop
        fc.blink(base, 'green')()
    
    elif value == 4: # playing
        fc.led_on(base, 'green')()
        fc.led_off([base+1, base+2])()
        fc.led_on(base+3, 'green')()
        
    elif value == 5: # overdubbing
        fc.led_on(base, 'green')()
        fc.led_on(base + 1, 'red')()
    
    elif value == 10: # muted
        # if muted, the other one is soloed...
        fc.led_off(base+3)()

    else:
        print("Unhandled message: %s, %s, %s" % (loop_index, control, value))


# End Sooperlooper
# ================

context.dpa_mul = .5
context.gated = False


context.reverb_osc_address = '/reverb/1'

# Common to BM & MA

from . import common

scenes = [scene+'.bm' for scene in common.scenes]
between = common.between

def setup():
    
    common.setup()
    sl_setup()
