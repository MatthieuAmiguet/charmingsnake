import pyo
from foococo import foococo as fc
import context

scenes = (
    'df_ouverture',
    'emergences',
    'fata_morgana',
    'abacus',
    'dragon_ivre',
    'steppe',
    'errances_obsessives',
    'finale',
)

def set_reverb(type):
    
    set_reverb.sender.send([type])

context.set_reverb = set_reverb

def setup():
    
    dpa_boost = pyo.SigTo(1, .1)
    dpa = pyo.Input(0, mul=context.dpa_mul*dpa_boost)
    if context.gated:
        dpa = pyo.Gate(dpa, thresh=-100)
    sennheiser = pyo.Input(1)
    fader = pyo.SigTo(0, .1, 0)
    mic_boost = pyo.SigTo(1, .1, 1)
    mic = pyo.Selector([sennheiser, dpa], voice=0, mul=fader*mic_boost)
    dry_level = pyo.SigTo(1, .1)
    dry_signal = mic * dry_level
    dry_signal.out(0)

    mic_send = pyo.SigTo(1, time=.1, init=1)
    global send
    send = pyo.Sig(mic, mul=mic_send).out(2)

    volume = pyo.Follower(dpa+sennheiser)
    thresh = pyo.Thresh(volume, .005)
    monitor = pyo.TrigFunc(thresh, fc.flash(10))

    b0 = fc.MultiState(
        fc.button(0),
        [
            [monitor.play, fc.led_on(0, 'red'), lambda: fader.setValue(0)],
            [monitor.stop, fc.led_on(0, 'green'), lambda: fader.setValue(1)],
        ],
    )

    b5 = fc.MultiState(
        fc.button(5),
        [
            [fc.led_on(5, 'green'), lambda:mic.set('voice', 0, .1)],
            [fc.led_on(5, 'red'), lambda:mic.set('voice', 1, .1)],
        ],
    )

    pedal = pyo.OscReceive(context.pedal_osc_port, '/pedal/1')

    set_reverb.sender = pyo.OscDataSend('s', 3456, context.reverb_osc_address)
    set_reverb('bypass')

    denorm = pyo.Noise(1e-24)

    context.mic = mic
    context.dpa = dpa
    context.sennheiser = sennheiser
    context.dry_level = dry_level
    context.mic_boost = mic_boost
    context.dpa_boost = dpa_boost
    context.mic_send = mic_send
    context.b0 = b0
    context.b5 = b5
    context.denorm = denorm
    context.pedal = pedal

between = fc.led_off([1,2,3,4,6,7,8,9])
