from functools import partial
from pythonosc.udp_client import SimpleUDPClient

bm_port = ("127.0.0.1", 7777)
ma_port = ("127.0.0.1", 7778)
sooloo_port = ("127.0.0.1", 9951)

bm = SimpleUDPClient(*bm_port)
ma = SimpleUDPClient(*ma_port)
sooloo = SimpleUDPClient(*sooloo_port)

noop = lambda x: ...

def send_vol(m):
    bm.send_message('/pedal/1', m)
    sooloo.send_message('/sl/-1/set', ['wet', m])

ped2sends = [
    send_vol,
    noop,
    partial(ma.send_message, '/pedal/1'),
    noop,
]

inits = [1, 0, 1, 0]
