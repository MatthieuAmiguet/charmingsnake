import sys
import pyo
from foococo import foococo as fc
from utils.sooperwrapper import SLFeedback
from utils.OSCSceneChanger import OSCSceneChanger
import context

try:
    # argv[1] will be the name of the track, paremeters start at 2
    ss_index = int(sys.argv[2])
except:
    print('Error: Missing parameter.')
    print('Usage: track ssn <n>')
    sys.exit(1)

scenes = [
    'Academy.sooperlooper',
    'Rivages.quickloops',
    'canon',
    'pachelbel.bm',
    'MA_solo.passamezzo',
]

jackname = f'ss{ss_index}'

controller_index = ss_index
controller_model = ss_index

# FIXME: determine the right channels
mic_in_channel = 4 if ss_index == 1 else 3
main_out_channel = 2 if ss_index == 1 else 1

loops = (1,0) if ss_index == 2 else (3,2)

in_ports = [
    [f'system:capture_{mic_in_channel}'], 
]
out_ports = [
    [f'system:playback_{main_out_channel}'], # main out
    [f'sooperlooper:loop{n}_in_1' for n in loops], # send to sooperlooper
]

init_message = 'POSTE 1' if ss_index == 2 else 'POSTE 2'

def setup():

    context.sc = OSCSceneChanger(9988 if ss_index == 1 else 9987)

    port = 7777 if ss_index == 2 else 7778
    context.pedal = pyo.SigTo(pyo.OscReceive(port, '/pedal/1'), .1)

    soo_port = 9901 if ss_index == 2 else 9902
    context.sooperlooper = SLFeedback(l1=loops[0], l2=loops[1], inport=soo_port)
    context.soo_loops = loops

    mic = pyo.Input(0)
    
    context.mic = mic
    context.ql_send = mic
    context.selectMic = lambda *args: ... # hack to avoid modifying scenes...

    context.nk2 = pyo.OscDataSend('ss', 4567, '/nk2/leds')

between = lambda: fc.led_off([1,2,3,4,6,7,8,9])()
