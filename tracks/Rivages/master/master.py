from beaupy import select
from rich.console import Console
import yaml
from websocket import create_connection
from pythonosc.udp_client import SimpleUDPClient

dB0 = 0.7647058823529421 # fader value for 0 dB
mic_inputs = [1,2,3,4,5,6,7, 10, 11]

people2mics = {
    'Santiago': 1,
    'Ben': 2,
    'Natasha': 3,
    'Pauline': 4,
    'Michelle': 5,
    'Barbara': 6,
    'Matthieu': 7,
    'poste1': 10,
    'poste2': 11,
}

def clean_data():

    global data
    data = { k: v if v is not None else {} for k, v in data.items() }

    for step, actions in data.items():

        actions['aux1'] = actions.get('aux1', [])
        actions['aux2'] = actions.get('aux2', [])
        actions['poste1'] = actions.get('poste1', None)
        actions['poste2'] = actions.get('poste2', None)
        actions['mugic'] = actions.get('mugic', None)

        actions['active'] = {
            people2mics[name]: value
            for name, value in actions.get('active', {}).items()
        }

        for aux in ['aux1', 'aux2']:
            actions[aux] = [
                people2mics[name]
                for name in actions.get(aux, [])
            ]


data = yaml.load(open('steps.yaml','r'), yaml.Loader)
clean_data()
options = list(data.keys())

main_host = 'march'
mugic_port = (main_host, 9986)
poste1_port = (main_host, 9987)
poste2_port = (main_host, 9988)

mugic = SimpleUDPClient(*mugic_port)
poste1 = SimpleUDPClient(*poste1_port)
poste2 = SimpleUDPClient(*poste2_port)

def make_changes(d):

    # mixer changes

    ws = create_connection("ws://ui24r/socket.io/1/websocket")

    for m in mic_inputs:

        to_activate = d['active']
        # main inputs mute + pan
        mute = 0 if m in to_activate else 1
        ws.send(f"3:::SETD^i.{m-1}.mute^{mute}")

        if not mute:
            pan = to_activate[m]
            ws.send(f'3:::SETD^i.{m-1}.pan^{pan}') # son direct
            ws.send(f'3:::SETD^i.{m+10}.pan^{pan}') # retour QL

        # aux sends are post-fader, which means
        # that the mute is not independent from
        # channel mute. Let's play on volumes instead...
        for l, s in (
            (d['aux1'], f"3:::SETD^i.{m-1}.aux.0.value^"),
            (d['aux2'], f"3:::SETD^i.{m-1}.aux.1.value^"),
        ):
            vol = dB0 if m in l else 0
            ws.send(s+f'{vol}')

    ws.close()

    # scene changes

    if scene := d['poste1']:
        poste1.send_message('/scene', [scene])

    if scene := d['poste2']:
        poste2.send_message('/scene', [scene])

    if scene := d['mugic']:
        mugic.send_message('/scene', [scene])
    else:
        mugic.send_message('/scene', ['IDLE'])


# main
console = Console()
current = -1

with console.screen():
    while True:
        o = options[:]
        if current >= 0:
            o[current] = f'[red bold]{o[current]}[/red bold]'
            cursor = current
        else:
            cursor = 0
        current = select(o, cursor_index=cursor, return_index=True)
        if current is None:
            break

        console.clear()
        ops = data[options[current]]
        try:
            make_changes(ops)
        except Exception as e:
            console.print("[red bold]Attention! Une erreur s'est produite![/red bold]\n")
            console.print(type(e).__name__, ': ', e)
            console.print('\n------------\n')


