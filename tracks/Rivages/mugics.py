import pyo
import context
from utils.OSCSceneChanger import OSCSceneChanger
from utils.footless_quickloop import QuickLoop
from utils.mugics_rivages import Mugics

in_ports = [
    [f'system:capture_{i}'] for i in range(5, 10)
]
out_ports = [
    [f'system:playback_{i}'] for i in range(3,8)
]
jackname = 'mugics'

scenes = [
    'Rivages.idle',
    'Rivages.rivages',
    'Rivages.duo1',
    'Rivages.harmonizer',
    'Rivages.duo2',
    'Rivages.vague',
]

def change_scene(_addr, scene_name):
    context.change_scene(short_name=scene_name)

def setup():
    context.sc = OSCSceneChanger(9986)

    context.mics = [pyo.Input(i) for i in range(5)]
    context.qls = [
        QuickLoop(mic, channel=i)
        for i, mic in enumerate(context.mics)
    ]
    context.mugics = Mugics()
