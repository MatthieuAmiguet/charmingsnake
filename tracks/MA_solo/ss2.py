import pyo
from foococo import foococo as fc
from utils.sooperwrapper import SLFeedback
import context

LOOPS_VOLUME = 1.2 # <--- Adjust loops volume here! :-)

scenes = [
    'MA_solo.sooperlooper',
    'MA_solo.quickloops',
    'MA_solo.mugic',
    'canon',
    # 'pachelbel.ma',
    'MA_solo.passamezzo',
    'snakin.ma',
]

jackname = 'ma_solo'

controller_index = 2
controller_model = 2

in_ports = [
    ['system:capture_1'], # contrabass dpa
    ['system:capture_4'], # sennheiser headset
    ['system:capture_3']  # bass dpa
]
out_ports = [
    ['system:playback_1', 'system:playback_3'], # main out, send to amp + reverb
    ['sooperlooper:loop0_in_1', 'sooperlooper:loop1_in_1'], # send to sooperlooper
]

init_message = 'FLUTES'

dpa_cb_mul = .35
dpa_b_mul = .2

def setup():

    context.pedal = pyo.SigTo(pyo.OscReceive(7777, '/pedal/1'), .1)

    context.sooperlooper = SLFeedback()

    context.boost = pyo.SigTo(1, .2, 1)
    
    dpa_cb = pyo.Input(0, mul=dpa_cb_mul)
    sennheiser = pyo.Input(1)
    dpa_b = pyo.Input(2, mul=dpa_b_mul)
    
    dpa = pyo.Selector([dpa_cb, dpa_b], voice=0, mul=context.boost)
    
    mic = pyo.Selector([sennheiser, dpa], voice=0, mul=0).out(0)
    sl_send = pyo.Sig(mic, mul=LOOPS_VOLUME).out(1) # send louder signal to sooperlooper
    ql_send = pyo.Sig(mic, mul=LOOPS_VOLUME) # send louder signal to quickloops

    volume = pyo.Follower(dpa+sennheiser)
    thresh = pyo.Thresh(volume, .005)
    monitor = pyo.TrigFunc(thresh, fc.flash(10))

    b0 = fc.MultiState(
        fc.button(0),
        [
            [monitor.play, fc.led_on(0, 'red'), lambda: mic.set('mul',0, .1)],
            [monitor.stop, fc.led_on(0, 'green'), lambda: mic.set('mul', 1, .1)],
        ],
    )


    # FIXME: this will probably be replaced by a physical expression pedal
    def selectMic(value, update_expr=False, display=True):
        
        if update_expr:
            expr.value = value
        
        mic.set('voice', value, port=0.2)
        
        if display:
            to_display = int((1 - value) * 100)
            fc.display('M')(to_display)
        
        if value == 0:
            fc.led_on(5, 'green')()
        elif value == 1:
            fc.led_on(5, 'red')()
        else:
            fc.led_off(5)()

    expr = fc.Expression(
            up = fc.button(5, 'b'),
            down = fc.button(5, 't'),
            callback = lambda val: selectMic(val/100.0),
            max_speed = .02,
            )

    b5 = [
        fc.Press(fc.button(5, 'l'), lambda: selectMic(1, update_expr=True), threshold=.95),
        fc.Press(fc.button(5, 'r'), lambda: selectMic(0, update_expr=True), threshold=.95),
        expr,
    ]
    
    selectMic(1)
    
    denorm = pyo.Noise(1e-24)

    context.dpa = dpa
    context.mic = mic
    context.sl_send = sl_send
    context.ql_send = ql_send
    context.b0 = b0
    context.b5 = b5
    context.denorm = denorm
    context.selectMic = selectMic

    context.nk2 = pyo.OscDataSend('ss', 4567, '/nk2/leds')

between = lambda: fc.led_off([1,2,3,4,6,7,8,9])()
