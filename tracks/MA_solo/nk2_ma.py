import time
import pyo
import nk2

server = pyo.Server(audio='jack', nchnls=0)
nk2.init(server)
server.boot()

# Set leds via OSC

def setleds(address, *args):

    led, action = args

    if action == 'on':
        nk2.led_on(led)
    elif action == 'off':
        nk2.led_off(led)
    elif action == 'flash':
        nk2.led_blink(led)

receive = pyo.OscDataReceive(4567, '/nk2/leds', setleds)


# Manage Sooperlooper sync (not nk2-related!)

inport = 9955
soopl = pyo.OscDataSend('sf', 9951, '/set')
states = [0] * 4

def loop_state(_address, *args):

    loop_index, control, value = args

    # If everything is stopped and I start recording on loop #1 or #3,
    # set sync to that loop
    if value == 2 and loop_index in [1, 3] and all([s in [0, 20] for s in states]):
        soopl.send(['sync_source', loop_index+1])

    states[loop_index] = value

    # If everything is stopped, set sync to None
    if all([s in [0, 20] for s in states]):
        soopl.send(['sync_source', 0])


register = pyo.OscDataSend(
    'siss', 
    9951, 
    [f'/sl/{i}/register_auto_update' for i in range(4)]
)
register.send(['state',100,f'localhost:{inport}', '/loop_state'])
get_state = pyo.OscDataReceive(inport, '/loop_state', loop_state)

# Reverb

def set_reverb(i, type):
    for j in range(4):
        nk2.led_off(f'r{j}')
    nk2.led_on(f'r{i}')
    set_reverb.sender.send([type])

set_reverb.sender = pyo.OscDataSend('s', 3456, '/reverb/1')
r0 = pyo.TrigFunc(nk2.Press("r0"), lambda: set_reverb(0, 'bypass'))
r1 = pyo.TrigFunc(nk2.Press("r1"), lambda: set_reverb(1, 's'))
r2 = pyo.TrigFunc(nk2.Press("r2"), lambda: set_reverb(2, 'm'))
r3 = pyo.TrigFunc(nk2.Press("r3"), lambda: set_reverb(3, 'l'))

# Main program

server.start()
nk2.leds_external()
time.sleep(.1)
set_reverb(0, 'bypass')

while True:
    time.sleep(1)
