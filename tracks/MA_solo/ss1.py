import pyo
from utils.sooperwrapper import SLFeedback, SLCommand
from foococo import foococo as fc
import context

scenes = [
]

controller_index = 1
controller_model = 1

jackname = 'ss1'

init_message = 'HRPJ'

def between(old, new):
    pass

in_ports = [
    ['system:capture_7'],
    ['system:capture_8'],
]
out_ports = [
    [
        'system:playback_8', # main out
        'system:playback_3', # reverb send
        'sooperlooper:loop2_in_1',
        'sooperlooper:loop3_in_1',
    ], 
    ['system:playback_7'], # tuner
]


# bass eq curve
eq_att_bass = [i * 1.2 for i in [
    0.98,
    0.74,
    1.67,
    2.00,
    1.07,
    1.26,
    1.10,
    1.15,
]]

eq_flat = [1] * 8

class Sustainer:

    def __init__(self, input, dur=.5, mul=1):

        self.buffer = pyo.NewTable(dur)
        self.bufrec = pyo.TableRec(input, self.buffer)
        self.sustain = pyo.Looper(self.buffer, dur=dur, xfade=[47,48,49,50], mul=mul).stop()
        self.out = pyo.Sig(self.sustain, mul=0)

    def on(self):

        print('on')
        self.bufrec.play(delay=.01),
        self.out.out(),
        self.out.set('mul', .5, .05),

    def off(self):

        print('off')
        self.out.set('mul', 0, .05, self.stop)

    def stop(self):
        self.out.stop()
        self.sustain.reset()
        self.buffer.reset()


def setup():

    context.sooperlooper = SLFeedback(l1=3, l2=2, inport=9902)
    context.sl_controls = SLCommand(l1=3, l2=2)
    context.sl_feedback = True
    context.all_loops = pyo.OscDataSend('s', 9951, '/sl/-1/hit')
    context.undo_all_global = fc.Press(fc.button('nav_up'), lambda: context.all_loops.send(['undo_all']))
    context.trig = fc.Press(fc.button(5), lambda: context.all_loops.send(['trigger']))

    context.pedals = pyo.OscReceive(7778, address=['/pedal/3', '/pedal/4'])
    ped3 = pyo.SigTo(context.pedals['/pedal/3'], time=.05, init=1)

    # harpejji
    # -------------


    # raw inputs

    raw_b = pyo.Input(0)
    raw_m = pyo.Input(1)

    # tuner
    tuner = pyo.Mix([raw_b, raw_m]).out(1)
    context.tuner = tuner

    # bass side
    eq = pyo.MultiBand(raw_b, num=8, mul=eq_att_bass)

    def b_eq_on():
        eq.set('mul', eq_att_bass, 0.05)

    def b_eq_off():
        eq.set('mul', eq_flat, 0.05)

    # b6 = fc.MultiState(
                # fc.button(6),
                # [
                    # [fc.led_on(6, 'green'), b_eq_on],
                    # [fc.led_on(6, 'red'), b_eq_off],
                # ],
            # )
    # context.b6 = b6

    bass = pyo.EQ(eq, freq=73.5, boost=-8)

    # melody side
    melody = raw_m

    # whole instrument :-)

    harpejji = pyo.Mix([bass, melody])
    harpejji.allowAutoStart(False)
    dry = pyo.Sig(harpejji, mul=ped3)

    # wah

    fol = pyo.SigTo(pyo.Follower(harpejji, freq=30, mul=10000, add=40), time=0.01)
    wah = pyo.Biquad(harpejji, freq=fol, q=5, type=2, mul=2.5)

    context.selector = pyo.Selector([dry, wah]).out()

    context.b0 = fc.MultiState(
        fc.button(0),
        [
            [fc.led_on(0,'red'), lambda: context.selector.set('voice', 0, .1)],
            [fc.led_on(0,'green'),  lambda: context.selector.set('voice', 1, .1)]
        ],
    )

    # sustain

    ped4 = context.pedals['/pedal/4']
    ped4_s = pyo.SigTo(context.pedals['/pedal/4'], time=.05, init=0)
    ped4_s.allowAutoStart(False)
    context.sus = Sustainer(harpejji, mul=ped4_s)
    context.sus_control = [
        pyo.TrigFunc(
            pyo.Thresh(ped4, .01, dir=0),
            context.sus.on
        ),
        pyo.TrigFunc(
            pyo.Thresh(ped4, .01, dir=1),
            context.sus.off
        )
    ]

    # # sustain2
    # st = 2**(1/12)
    # wgi = pyo.Waveguide(pyo.Gate(harpejji, -100), [55 * st**i for i in range(48)])
    # wg = pyo.Compress(wgi.mix(1), thresh=-60, mul=pyo.Fader(.1, .1,mul=.3))
    # # context.wg.ctrl(wxnoserver=True)
    # b4 = [
        # fc.Press(fc.button(4), [fc.led_on(4, 'green'), wgi.reset, wg.out]),
        # fc.Release(fc.button(4), [fc.led_on(4, 'green'), lambda: wg.stop(.1)])
    # ]
    # context.b4 = b4
