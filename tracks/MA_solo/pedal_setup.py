from functools import partial
from pythonosc.udp_client import SimpleUDPClient

flutes_port = ("127.0.0.1", 7777)
harpejji_port = ("127.0.0.1", 7778)
sooloo_port = ("127.0.0.1", 9951)

flutes = SimpleUDPClient(*flutes_port)
harpejji = SimpleUDPClient(*harpejji_port)
sooloo = SimpleUDPClient(*sooloo_port)

ped2sends = [
    partial(flutes.send_message, '/pedal/1'),
    lambda v: sooloo.send_message('/sl/-1/set', ['wet', v]),
    partial(harpejji.send_message, '/pedal/3'),
    partial(harpejji.send_message, '/pedal/4'),
]

inits = [1, 1, 1, 0]
