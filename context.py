''''

This is kind of a bloackboard for other modules to share information.

It has a "magic" feature, though: if you add a PyoObject (or other object with a
.allowAutoStart method), it will call .allowAutoStart(False) before storing the
object. The rationale is that if the object is in context, it is likely to be
used in several places, so you probably don't want it to be auto-stopped.

'''

class Context(object):
    
    def __setattr__(self, prop, val):
        
        try:
            val.allowAutoStart(False)
        except AttributeError:
            pass
        
        object.__setattr__(self, prop, val)

# This trick seems the "recommended" way of customizing
# attribute access on a module:
# https://stackoverflow.com/questions/2447353/getattr-on-a-module
import sys
sys.modules[__name__] = Context()
